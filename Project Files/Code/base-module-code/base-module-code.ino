#include "lcd.h"
#include "num_keypad.h"
#include "i2c_master.h"
#include "rotary_encoder.h"
#include "constants.h"
#include <iostream>
#include <string>
using namespace std;

char reel_name[] = "Reel ";
string temp = "";
char key_entry = 'a';

uint8_t current_reel_selection;
bool errorPollFlag = false;
string keypad_entry_str;
uint16_t keypad_entry_int, repeat = 0;

void setup() {
  uint8_t reel_count;
  Serial.begin(115200);
  delay(1000);
  Serial.println();
  Serial.println("configuring...");

  init_lcd();
  Serial.println("LCD done");

  i2c_setup();
  Serial.println("i2c_done");

  pinMode(17, OUTPUT);
  digitalWrite(17, 0);
  encoder_setup();
  pinMode(8, OUTPUT);
  digitalWrite(8, 0);
  Serial.println("encoder DONE");

  delay(100);
  Serial.println("START!!!!");
  current_reel_selection = 0;
  keypad_entry_str = "";
  keypad_entry_int = 0;
  write_string("Detecting Reels");
  delay(2000);
}

void readValFromScreen(){
  next_line();
  key_entry = 'a';
  while (1) {
    reset_cursor(0, 1);
    key_entry = getPressedKey();
    if (key_entry == '#' && keypad_entry_str != "") {
      break;
    }
    else if(key_entry == '#' && keypad_entry_str == ""){
      continue;
    }
    else if (key_entry == '0' && keypad_entry_str == "" && attached_reels[current_reel_selection].spacing_set == true) {
      attached_reels[current_reel_selection].spacing_set = false;
      break;
    }
    else if (key_entry == '*') {
      break;
    }
    else if(key_entry == '0' && keypad_entry_str == "" && attached_reels[current_reel_selection].spacing_set == false){
      continue;
    }
    keypad_entry_str += key_entry;
    write_string(keypad_entry_str.c_str());
  }
}

void everythingFunction(uint8_t current_reel_selection) {
  do {
    for (uint8_t errorPollIdx = 0; errorPollIdx < REELS_ATTACHED; errorPollIdx++){
      if(errorPoll(errorPollIdx)){
        break;
      }
    }
    if (isEncoderPressed()) {
      if (!attached_reels[current_reel_selection].spacing_set) {
        clear_lcd();
        write_string("Enter Spacing:");
        readValFromScreen();
        if (key_entry == '*') {
          keypad_entry_int = 0;
          key_entry = 'a';
          keypad_entry_str = "";
          break;
        }
        keypad_entry_int = stoi(keypad_entry_str);
        if(errorPoll(current_reel_selection)){
          keypad_entry_int = 0;
          key_entry = 'a';
          keypad_entry_str = "";
          break;
        }
        i2c_write(current_reel_selection, 0, keypad_entry_int);
        attached_reels[current_reel_selection].spacing = keypad_entry_int;
        keypad_entry_int = 0;
        key_entry = 'a';
        keypad_entry_str = "";
        attached_reels[current_reel_selection].spacing_set = true;
      }

      clear_lcd();
      write_string("Enter Repeat:");
      readValFromScreen();
      if (key_entry == '0' && keypad_entry_str == ""){
        key_entry = 'a';
        break;
      }
      if (key_entry == '*') {
        keypad_entry_int = 0;
        key_entry = 'a';
        keypad_entry_str = "";
        break;
      }
      keypad_entry_int = stoi(keypad_entry_str);
      repeat = keypad_entry_int;
      keypad_entry_int = 0;
      key_entry = 'a';
      keypad_entry_str = "";
      if(errorPoll(current_reel_selection)){
        repeat = 0;
        break;
      }

      clear_lcd();
      write_string("Quantity: MAX:");
      int tempQuant = (int)(PHYSICAL_SPACING/attached_reels[current_reel_selection].spacing); // limit spacing to physical space/reel spacing
      write_string(to_string(tempQuant).c_str());
      readValFromScreen();
      if (key_entry == '0' && keypad_entry_str == ""){
        key_entry = 'a';
        break;
      }
      if (key_entry == '*') {
          keypad_entry_int = 0;
          key_entry = 'a';
          keypad_entry_str = "";
          break;
        }
      keypad_entry_int = stoi(keypad_entry_str);
      while(repeat > 0){
        if(errorPoll(current_reel_selection)){
          repeat = 0;
          errorPollFlag = true;
          break;
        }
        i2c_write(current_reel_selection, keypad_entry_int, 0);
        repeat--;
      }
      keypad_entry_int = 0;
      keypad_entry_str = "";
      if(errorPollFlag){
        key_entry = 'a';
        errorPollFlag = false;
        break;
      }
      if (key_entry == '#') {
        key_entry = 'a';
        break;
      }
    }
  } while (!isEncoderChanged());
}

void loop() {
  clear_lcd();

  if (REELS_ATTACHED == 0) {
    write_string("No Reels");
    while (1);
  }

  write_string(reel_name);

  temp = to_string((current_reel_selection + 1));
  write_string(temp.c_str());  

  /* displays spacing if already set or read in for the module along with name*/

  if(attached_reels[current_reel_selection].spacing_set){
    next_line();
    write_string("Space:");
    write_string(to_string(attached_reels[current_reel_selection].spacing).c_str());
  }

  everythingFunction(current_reel_selection);

  current_reel_selection = (unsigned int)getCurrentEncoderValue();
  // show reel name on line 1 and if rotary encoder switch pressed then ask for quantity(spacing too first time around) with numeric value on second line quantity
  // else if rotary encoder knob is turned then display next reel's name in Line 1 and same logic for quantity/spacing and so forth and vice-versa

  // once switch is pressed wait for keypad press, take input of numbers until '#' is pressed on keyoad then pass that number along on the right i2c address in the array
  // everytime check if * is pressed then cancel the operation and act as a back button
}
