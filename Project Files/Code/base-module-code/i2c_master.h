#pragma once

#include "constants.h"

void i2c_setup();
void stopOperations(uint8_t addr_idx);
bool errorPoll(uint8_t addr_idx);
void i2c_write(uint8_t addr_idx, uint8_t quantity, uint16_t spacing);
void error_check(uint8_t err);
uint8_t i2c_write_quantity_with_spacing(uint8_t addr_idx, uint8_t quantity, uint16_t spacing);
uint8_t i2c_write_only_quantity(uint8_t addr_idx, uint8_t quantity);
uint8_t i2c_write_only_spacing(uint8_t addr_idx, uint16_t spacing);
bool i2c_read(uint8_t addr_idx, int readRegisterCmd);

#define STATUS_REGISTER_CMD 0x00
#define QUANTITY_REGISTER_CMD 0x01
#define SPACING_REGISTER_LB_CMD 0x02
#define SPACING_REGISTER_UB_CMD 0x03

#define ERROR_BIT 0x01
#define RUN_BIT 0x02
#define READ_SIZE 1

#define VOLTAGE_LOW 0x01
#define CUTTER_STALL 0x02

struct Reel {             
  uint8_t addr;         
  uint16_t spacing;
  bool spacing_set;
};

extern struct Reel attached_reels[MAX_NUM_REELS];
extern uint8_t read_arr[READ_SIZE];