#pragma once

#include <LiquidCrystal.h>

void write_string(const char *str);
void init_lcd();
void clear_lcd();
void next_line();
void reset_cursor(uint8_t x, uint8_t y);