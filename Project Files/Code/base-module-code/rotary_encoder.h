#pragma once
#include <AiEsp32RotaryEncoder.h>
#include <AiEsp32RotaryEncoderNumberSelector.h>

void encoder_setup();
bool isEncoderPressed();
bool isEncoderChanged();
uint8_t getCurrentEncoderValue();