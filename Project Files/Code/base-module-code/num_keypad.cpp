#include <Keypad.h>
#include "constants.h"

#define ROW1 36
#define ROW2 41
#define ROW3 40
#define ROW4 38
#define COL1 37
#define COL2 35
#define COL3 39

const byte ROWS = 4; /* four rows */
const byte COLS = 3; /* three columns */

/* define the symbols on the buttons of the keypads */
char hexaKeys[ROWS][COLS] = {
  { '1', '2', '3' },
  { '4', '5', '6' },
  { '7', '8', '9' },
  { '*', '0', '#' }
};

byte rowPins[ROWS] = { ROW1, ROW2, ROW3, ROW4 }; /* connect to the row pinouts of the keypad */
byte colPins[COLS] = { COL1, COL2, COL3 };       /* connect to the column pinouts of the keypad */

/* initialize an instance of class NewKeypad */
Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

char getPressedKey() {
  return customKeypad.waitForKey();
}
char getKeyNoWait() {
  return customKeypad.getKey();
}
