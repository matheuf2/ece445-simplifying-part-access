#include <AiEsp32RotaryEncoder.h>
#include <AiEsp32RotaryEncoderNumberSelector.h>
#include "constants.h"
#include "rotary_encoder.h"

#define ROTARY_ENCODER_A_PIN 16
#define ROTARY_ENCODER_B_PIN 15
#define ROTARY_ENCODER_BUTTON_PIN 18
#define ROTARY_ENCODER_STEPS 12
#define VCC_PIN  8

AiEsp32RotaryEncoder *rotaryEncoder = new AiEsp32RotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, -1, ROTARY_ENCODER_STEPS);
AiEsp32RotaryEncoderNumberSelector numberSelector = AiEsp32RotaryEncoderNumberSelector();

void IRAM_ATTR readEncoderISR(){
    rotaryEncoder->readEncoder_ISR();
}

void encoder_setup(){
    rotaryEncoder->begin();
    rotaryEncoder->setup(readEncoderISR);
    numberSelector.attachEncoder(rotaryEncoder);
    if(REELS_ATTACHED != 0){
        numberSelector.setRange(0,REELS_ATTACHED-1, 1, true, 0);
    }
    else{
        numberSelector.setRange(0,0, 1, true, 0);
    }
    numberSelector.setValue(0);
}

bool isEncoderPressed(){
    return rotaryEncoder->isEncoderButtonClicked();
}
bool isEncoderChanged(){
    return rotaryEncoder->encoderChanged();
}

uint8_t getCurrentEncoderValue(){
    return (uint8_t)numberSelector.getValue();
}