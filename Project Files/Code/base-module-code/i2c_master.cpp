#include <Wire.h>
#include <Arduino.h>
#include <iostream>
#include <string>
#include "constants.h"
#include "i2c_master.h"
#include "num_keypad.h"
#include "lcd.h"
using namespace std;

#define SDA 46
#define SCL 3

#define I2C_DEFAULT_ADDR 0x7F
#define I2C_FIRST_DEVICE_ADDR 0x01

Reel attached_reels[MAX_NUM_REELS];
uint8_t read_arr[READ_SIZE];
uint8_t REELS_ATTACHED = 0;
uint8_t tempErrorPoll = 0;

void error_check(uint8_t err){
   if(err == 1){
    Serial.println("I2C Error 1 : Data too long");
  }
  else if(err == 2){
    Serial.println("I2C Error 2 : NACK on address");
  }
  else if(err == 3){
    Serial.println("I2C Error 3 : NACK on data");
  }
  else if(err == 4){
    Serial.println("I2C Error 4 : Unknown Error");
  }
  else if(err == 5){
    Serial.println("I2C Error 5 : Timeout");
  }
}

// WRITTEN read the value from the reel module and see if there is a spacing stored in setup. If there is then use that value and store it in the array and set flag to true. 
// WRITTEN Also add functionality to edit spacing after it is set.
//WRITTEN  have a quantity and repeat . Check run bit and if it is 0 then send i2c command for quantity again in loop based on repeat.

// WRITTEN and TESTED send calibrate commmand when setting address

// WRITTTEN spacing is zero

// WRITTEN limit quantity based on spacing

//optional think how multiple reels can be run at the same time

// WRITTEN have constant polling on each reel module for it's error register.

void i2c_setup(){
  uint8_t i = 0;
  uint8_t error = 0;
  uint16_t tempRead = 0x0000;
  Serial.setDebugOutput(true);
  Wire.begin(SDA, SCL, 100000);
  
  while(error == 0){
    Wire.beginTransmission(I2C_DEFAULT_ADDR);
    Wire.write(I2C_FIRST_DEVICE_ADDR+i);
    error = Wire.endTransmission(true);
    if(error != 0){
      break;
    }
    Reel new_reel = {I2C_FIRST_DEVICE_ADDR+i, 0, false};
    attached_reels[i] = new_reel;
    
    /* send calibrate command */
    delay(2000);
    Wire.beginTransmission(attached_reels[i].addr);
    Wire.write(STATUS_REGISTER_CMD);
    Wire.write(0x04);
    Wire.endTransmission();

    /* check if spacing is already stored then read it and then set it */
    if(i2c_read(i, SPACING_REGISTER_UB_CMD)){
      tempRead = read_arr[0];
      tempRead<<=8;
    }
    if(i2c_read(i, SPACING_REGISTER_LB_CMD)){
      tempRead |= read_arr[0];
    }
    if(tempRead != 0x0000){
      attached_reels[i].spacing = tempRead;
      attached_reels[i].spacing_set = true;
    }

    i++;
    REELS_ATTACHED = i;
  }
  error_check(error);
}

/* cancel all operations and set the */
void stopOperations(uint8_t addr_idx){
  if(getKeyNoWait() == '*'){
    Wire.beginTransmission(attached_reels[addr_idx].addr);
    Wire.write(STATUS_REGISTER_CMD);
    Wire.write(RUN_BIT);
    Wire.endTransmission();
  }
}

bool errorPoll(uint8_t addr_idx){
  if(i2c_read(addr_idx, STATUS_REGISTER_CMD)){
    if(read_arr[0] & ERROR_BIT){
      clear_lcd();
      write_string("To clr ");
      tempErrorPoll = read_arr[0];
      tempErrorPoll>>=4;
      if(tempErrorPoll == VOLTAGE_LOW){
        write_string("Vlt");
      }
      else if(tempErrorPoll == CUTTER_STALL){
        write_string("Stl");
      }
      write_string(" Error");
      next_line();
      write_string("on ");
      write_string(to_string(addr_idx).c_str());
      write_string(",press #");
      while(1){
        if(getPressedKey() == '#'){
          Wire.beginTransmission(attached_reels[addr_idx].addr);
          Wire.write(STATUS_REGISTER_CMD);
          Wire.write(ERROR_BIT);
          Wire.endTransmission();
          break;
        }
      }
      return true;
    }
  }
  return false;
}

uint8_t i2c_write_only_quantity(uint8_t addr_idx, uint8_t quantity){
  Wire.beginTransmission(attached_reels[addr_idx].addr);
  Wire.write(QUANTITY_REGISTER_CMD);
  Wire.write(quantity);
  return Wire.endTransmission();
}

uint8_t i2c_write_only_spacing(uint8_t addr_idx, uint16_t spacing){
  uint8_t lsb = spacing & 0x00FF;
  uint8_t msb = (spacing & 0xFF00) >> 8;
  Wire.beginTransmission(attached_reels[addr_idx].addr);
  Wire.write(SPACING_REGISTER_LB_CMD); // lower byte is 0x02 and upper is 0x03
  Wire.write(lsb);
  Wire.write(SPACING_REGISTER_UB_CMD);
  Wire.write(msb);
  return Wire.endTransmission();
}

uint8_t i2c_write_quantity_with_spacing(uint8_t addr_idx, uint8_t quantity, uint16_t spacing){
  uint8_t spacing_error = i2c_write_only_spacing(addr_idx,spacing);
  if(spacing_error != 0){
    return spacing_error;
  }
  return i2c_write_only_quantity(addr_idx, quantity);
}

void i2c_write(uint8_t addr_idx, uint8_t quantity, uint16_t spacing){

  stopOperations(addr_idx);

  /* code to wait until reel is not busy before sending a write command*/
  while(1){
    stopOperations(addr_idx);
    Serial.println("Currently selected reel is busy");
    Serial.print("Currently Selected Reel: ");
    Serial.println(addr_idx);
    if(i2c_read(addr_idx, STATUS_REGISTER_CMD)){
      if(!(read_arr[0] & RUN_BIT)){
        stopOperations(addr_idx);
        break;
      }
    }
  }
  uint8_t write_error;
  if(quantity !=0 && spacing !=0){
    write_error = i2c_write_quantity_with_spacing(addr_idx, quantity, spacing);
    attached_reels[addr_idx].spacing = spacing;
  }
  else if(spacing == 0){
    write_error = i2c_write_only_quantity(addr_idx, quantity);
  }
  else if(quantity == 0){
    write_error = i2c_write_only_spacing(addr_idx, spacing);    
    attached_reels[addr_idx].spacing = spacing;
  }
  else{
    Serial.println("UNEXPECT ERROR OCCURED in I2C_WRITE");
  }
  error_check(write_error);
}

bool i2c_read(uint8_t addr_idx, int readRegisterCmd){
  Wire.beginTransmission(attached_reels[addr_idx].addr);
  Wire.write(readRegisterCmd);
  uint8_t error = Wire.endTransmission();
  error_check(error);
  uint8_t bytesReceived = Wire.requestFrom(attached_reels[addr_idx].addr, READ_SIZE);
  Serial.printf("bytesReceived on requestFrom: %u\n", bytesReceived);
  if((bool)bytesReceived && bytesReceived == READ_SIZE){ //If received more than zero bytes
    Wire.readBytes(read_arr, bytesReceived);
    return true;
    // log_print_buf(temp, bytesReceived);
  }
  return false;
}


// WRITTEN write register index and then read from that register
// WRITTEN read status byte with bit 0 is error bit and if it is 1 then inform user that operation has stopped on that module, give user option of selection yes or no if error is cleared
// WRITTEN if it cleared then send 1 on the error bit to clear it - check error bit periodically for all slave modules
// WRITTEN run bit is bit 1 and if it is high then no new operations will be allowed until it goes to zero - check it when user does print command
// WRITTEN read and show the spacing when reel is selected as regular 
