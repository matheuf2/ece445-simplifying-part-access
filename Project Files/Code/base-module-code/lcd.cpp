#include "lcd.h"
#include <LiquidCrystal.h>
#include "constants.h"

const int rs = 11, en = 45, d4 = 12, d5 = 1, d6 = 2, d7 = 10;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void init_lcd(){
   lcd.begin(16, 2);
   lcd.setCursor(0, 0);
}

void write_string(const char *str){
  lcd.printf(str);
}

void clear_lcd(){
  lcd.clear();
}

void next_line(){
  lcd.setCursor(0,1);
}

void reset_cursor(uint8_t x, uint8_t y){
  lcd.setCursor(x, y);
}