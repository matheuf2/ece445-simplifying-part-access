#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/atomic.h>

#include "pins.h"
#include "led_disp.h"
#include "timing.h"
#include "analog.h"
#include "motor.h"
#include "stepper.h"
#include "i2c.h"
#include "eeprom.h"

// timer ISR handler function from timing.h
// runs at 1kHz
void timer1_handler(void)
{
    // update the display
    led_disp_update();
}

// I2C registers

// Status register: bit field of various status/control options
// 7:4 | R | Error code (TBD). Updated when the error status is set to 1.
//  3  | x | Reserved
//  2  |R/W| Calibration control. Write a 1 bit to start; stops automatically in a few seconds (read for status).
//  1  |R/W| Run status. If running (1), write a 1 bit to cancel the current operation (including calibration).
//  0  |R/W| Error status. If set, the error code bits contain the cause of the error. Write 1 to clear and restart
//           the system.

uint8_t  status_reg  = 0;

// values written by the i2c handler
uint8_t  cut_reg     = 0; // number of components to advance before cutting (starts a cut operation)
uint16_t spacing_reg = 0; // spacing per component in 1/100 mm units (can only be set while not running)

int32_t enc_reg      = 0; // last sampled encoder count

uint8_t  update_flags = 0; // bit flags when any register is changed by the i2c peripheral

// actual values used in the code
uint8_t cut_count = 0;
uint16_t spacing = 0;


// status bit flags
#define STATUS_ERR      0x01 // device has an error and the motors are stopped, write 1 to return to WAIT_CMD
#define STATUS_RUN      0x02 // device is running a cut operation, write 1 to this bit to cancel
#define STATUS_CAL      0x04 // run auto-calibration (write 1 to start and 0 to cancel)

// update flags
#define UPDATE_RUN      0x01 // status register RUN bit updated
#define UPDATE_CAL      0x02 // calibration bit set
#define UPDATE_CUT      0x04 // cut count updated
#define UPDATE_SPACING  0x08 // spacing updated (write back to eeprom)
#define UPDATE_ERR      0x10 // error code updated

// register addresses
#define REG_STATUS      0x00
#define REG_CUT         0x01
#define REG_SPACING_L   0x02
#define REG_SPACING_H   0x03 // write the low byte first (writing the high byte commits the value to memory
// debug (read-only) registers
#define REG_ENC_L       0x04
#define REG_ENC_1       0x05
#define REG_ENC_2       0x06
#define REG_ENC_H       0x07
#define REG_CTRL_STATE  0x08
#define REG_UPD_FLAGS   0x09

// I2C functions from i2c.h
void i2c_write_handler(uint8_t reg, uint8_t val)
{
    switch (reg)
    {
        case REG_STATUS:
            // update bits if allowed
            if (val & STATUS_RUN)
                update_flags |= UPDATE_RUN;
            if (val & STATUS_CAL)
                update_flags |= UPDATE_CAL;
            if (val & STATUS_ERR)
                update_flags |= UPDATE_ERR;
            break;
        case REG_CUT:
            // update cut count
            cut_reg = val;
            update_flags |= UPDATE_CUT;
            break;
        case REG_SPACING_L:
            // update spacing low byte
            spacing_reg = val;
            break;
        case REG_SPACING_H:
            // update spacing high byte
            spacing_reg |= (val << 8);
            update_flags |= UPDATE_SPACING;
            break;
    }
}

uint8_t i2c_read_handler(uint8_t reg)
{
    switch (reg)
    {
        case REG_STATUS:
            return status_reg;
        case REG_CUT:
            return cut_count;
        case REG_SPACING_L:
            return (uint8_t)spacing;
        case REG_SPACING_H:
            return (uint8_t)(spacing >> 8);
        case REG_ENC_L:
            return (uint8_t)(enc_reg);
        case REG_ENC_1:
            return (uint8_t)(enc_reg >> 8);
        case REG_ENC_2:
            return (uint8_t)(enc_reg >> 16);
        case REG_ENC_H:
            enc_reg = motor_get_encoder_count();
            return (uint8_t)(enc_reg >> 24);
        default:
            return 0;
    }
}

static uint8_t load_eeprom_data(void)
{
    // note: do this before I2C is initialized
    
    // EEPROM layout:
    // 000 : spacing L
    // 001 : spacing H
    // 002 : checksum (0x55 + sum of spacing bytes)
    uint8_t data[2];
    uint8_t checksum;
    data[0] = eeprom_read_byte(0x00);
    data[1] = eeprom_read_byte(0x01);
    checksum = eeprom_read_byte(0x02);
    
    uint8_t sum = 0x55;
    for (uint8_t i = 0; i < sizeof(data); i++) sum += data[i];
    if (sum != checksum)
    {
        spacing = 0; // no spacing
        return 0; // fail
    }

    spacing = data[0] | (data[1] << 8);

    return 1; // ok
}

static void save_eeprom_data(void)
{
    uint8_t data[2];
    uint8_t checksum;
    data[0] = (spacing & 0xff);
    data[1] = ((spacing >> 8) & 0xff);

    checksum = 0x55;
    for (uint8_t i = 0; i < sizeof(data); i++) checksum += data[i];

    eeprom_write_byte(0, data[0]);
    eeprom_write_byte(1, data[1]);
    eeprom_write_byte(2, checksum);
}

static uint8_t get_bcd(uint8_t v)
{
    uint8_t bcd = (v % 10);
    bcd |= ((v / 10) % 10) << 4;
    return bcd;
}

static inline void buttons_init(void)
{
    // set pull-up resistors
    WRITE_PIN(BUTTON_A_PORT, BUTTON_A_PIN, 1);
    WRITE_PIN(BUTTON_B_PORT, BUTTON_B_PIN, 1);
}

static inline uint8_t get_btn_a(void)
{
    return !READ_PIN(BUTTON_A_PORT, BUTTON_A_PIN);
}

static inline uint8_t get_btn_b(void)
{
    return !READ_PIN(BUTTON_B_PORT, BUTTON_B_PIN);
}

#define CTRL_WAIT_I2C   0
#define CTRL_WAIT_CMD   1

#define CTRL_DO_STEP    2
#define CTRL_DO_CUT     3
#define CTRL_DO_CUT_REV 4

#define CTRL_FIND_STOP  5

#define CTRL_ERROR      6

#define CTRL_STALL_REC  7               // recover from stall and then raise an error

#define CTRL_FLAG_CALIBRATED 0x01       // set if the cutter is calibrated
#define CTRL_FLAG_12V_LOW    0x02       // set if the input voltage is too low
#define CTRL_FLAG_ENC_STALL  0x04       // set if the cutter is stalled 

// errors
#define ERR_LOW_12V     0x1             // 12v voltage too low for proper function
#define ERR_CUT_STALL   0x2             // cutter stalled unexpectedly

uint8_t ctrl_state = CTRL_WAIT_I2C;
uint8_t ctrl_flags = 0;

int32_t stepper_target_remainder = 0;   // remainder from converting 1/100mm to stepper steps

uint32_t v_low_start = 0;               // start time of low voltage reading
uint32_t enc_sample_last = 0;           // last time the encoder was sampled
uint32_t enc_stall_start = 0;           // start time of stall detection
int32_t last_enc_value = 0;

uint32_t gp_time_last = 0;              // time counter for anything that needs it

/******************/
// constants that we need to figure out

#define CUT_DIR         M_REVERSE       // direction to move the motor to perform a cut
#define CUT_PWR         255             // motor power to use when actually cutting the tape
#define CUT_DISTANCE    3600            // number of encoder ticks to move from the idle position to the end stop
#define CUT_STOP        150
#define CAL_PWR         80              // lower power for finding the zero without destroying things (/255)
#define DEBUG_PWR       255

// conversion from distance (in 1/100 mm) to stepper steps
// optimized to use integer division because floating poing operations are going to be *slow* (probably <1kFLOP)
// conversion = 0.0242 steps per 0.01mm
#define MM_TO_STEP_MUL  3329            // steps = (mm * MUL) / DIV
#define MM_TO_STEP_DIV  137536          // see above

#define STEP_REVERSE    0               // whether to reverse the normal step direction
#define STEP_RPM        20              // stepper RPM
#define MICROSTEP       4               // how many microsteps

#define V_LOW_TIMEOUT   200             // milliseconds of continuous low-voltage glitch before an error is raised

#define CAL_STOPPED_T   10000           // timeout if the motor is stalled when calibrating (hit end stop)
#define CAL_STOP_DELTA  5               // how much the encoder can vary and still be "stalled"
#define CAL_SAMPLE_T    10              // how many ms between reading the encoder

// 150 RPM (1m/60s)(360deg/R) = 900 deg/s (1 step/0.9 deg)(1s/1e6 us) = 1000 step/s = 0.001 step/us
// 1 RPM = 6 deg/s = 6/9e-5 step/us => 1/(1RPM) = 1.5e5 us/step
// this should have been immediately obvious but my brain is being slow today
// cast to uint32_t so we can have non-integer RPM (the floating point math will be optimized away)
#define STEP_PERIOD     ((uint32_t)(150000 / STEP_RPM))

/*
static void debug_print_u32(uint32_t x)
{
    led_disp_set_raw(0x1010);
    _delay_ms(1000);
    for (int8_t i = 3; i >= 0; --i)
    {
        led_disp_set_hex(x >> (8*i));
        _delay_ms(1000);
        led_disp_set_raw(0);
        _delay_ms(100);
    }
}
*/

static void add_step(uint8_t n)
{
    int32_t delta = (int32_t)spacing * n * MM_TO_STEP_MUL * MICROSTEP;
    int32_t delta_step = delta / MM_TO_STEP_DIV;
    int32_t delta_remainder = delta % MM_TO_STEP_DIV;

    // debug_print_u32((uint32_t)spacing);
    // debug_print_u32((uint32_t)delta);
    // debug_print_u32((uint32_t)delta_step);
    // debug_print_u32((uint32_t)delta_remainder);

    stepper_target_remainder += delta_remainder;
    while (stepper_target_remainder >= MM_TO_STEP_DIV/2)
    {
        stepper_target_remainder -= MM_TO_STEP_DIV;
        delta += 1;
    }

    if (STEP_REVERSE) delta_step = -delta_step;
    // debug_print_u32((uint32_t)delta_step);

    stepper_step(delta_step);
}

static void do_error(uint8_t code)
{
    // stop everything
    motor_set_power(0);
    stepper_stop();
    stepper_enable(0);
    // set the status to the error
    status_reg = (code << 4) | STATUS_ERR;
    ctrl_state = CTRL_ERROR;
}

static uint8_t check_run_errors(uint8_t flags)
{
    // make sure everything is ok to keep running the motors/steppers
    // used for DO_STEP, DO_CUT, DO_CUT_REV, and FIND_STOP
    // returns 1 if the state changed
    if (flags & UPDATE_RUN)
    {
        update_flags = 0;
        // stop the stepper
        stepper_stop();
        stepper_enable(0);
        // stop the motor
        motor_set_power(0);
        // go back to WAIT_CMD
        status_reg &= ~(STATUS_RUN | STATUS_CAL);
        ctrl_state = CTRL_WAIT_CMD;
        return 1;
    }
    else if (flags)
    {
        update_flags = 0; // clear any other flags
    }

    // check if voltage is ok
    check_vsense();
    if (!vsense_ok())
    {
        if (ctrl_flags & CTRL_FLAG_12V_LOW)
        {
            led_disp_set_hex(get_bcd(get_vsense_mv() / 1000));
            if (millis() - v_low_start >= V_LOW_TIMEOUT)
            {
                // jump to the error state
                ctrl_flags &= ~(CTRL_FLAG_12V_LOW); // clear error status
                do_error(ERR_LOW_12V);
                return 1;
            }
        }
        else
        {
            ctrl_flags |= CTRL_FLAG_12V_LOW;
            v_low_start = millis();
        }
    }
    else
    {
        ctrl_flags &= ~(CTRL_FLAG_12V_LOW);
    }

    return 0;
}

static uint8_t check_enc_stall(void)
{
    if (millis() - enc_sample_last >= CAL_SAMPLE_T)
    {
        enc_sample_last = millis();
        int32_t enc = motor_get_encoder_count();
        int32_t delta = enc - last_enc_value;
        if (delta < CAL_STOP_DELTA && delta > -CAL_STOP_DELTA)
        {
            // stall detected
            if ((ctrl_flags & CTRL_FLAG_ENC_STALL))
            {
                if (millis() - enc_stall_start >= CAL_STOPPED_T)
                {
                    ctrl_flags &= ~(CTRL_FLAG_ENC_STALL);
                    return 1; // enc is in fact stalled, yay
                }
            }
            else
            {
                enc_stall_start = millis();
                ctrl_flags |= CTRL_FLAG_ENC_STALL;
            }
        }
        else
        {
            last_enc_value = enc;
            ctrl_flags &= ~(CTRL_FLAG_ENC_STALL);
        }
    }
    return 0;
}

static void start_cal(void)
{
    // start a calibration operation
    status_reg |= STATUS_CAL;
    status_reg |= STATUS_RUN;
    gp_time_last = millis();
    ctrl_state = CTRL_FIND_STOP;
}

static void start_cut(void)
{
    // start a cut operation to pull cut_count components and cut them
    // runs calibration if not already calibrated
    if (!(ctrl_flags & CTRL_FLAG_CALIBRATED))
    {
        // the calibration operation will hold onto our UPDATE_CUT flag until we get back
        start_cal();
        return;
    }

    update_flags = 0;
    cut_count = cut_reg;
    status_reg |= STATUS_RUN;
    // start the stepper motor
    stepper_enable(1);

    ctrl_state = CTRL_DO_CUT_REV; // lift the cutter and start a cut operation
}

void do_final_sm(void)
{
    uint8_t flags = update_flags;
    switch (ctrl_state)
    {
        case CTRL_WAIT_I2C:
            // WAIT_I2C - hold until the control module gives an address
            // - cannot receive any commands in this state
            // - no motors enabled in this state
            /*
            if (get_btn_b()) motor_set_power(DEBUG_PWR);
            else if (get_btn_a()) motor_set_power(-DEBUG_PWR);
            else motor_set_power(0);
            */
            if (i2c_negotiated())
            {
                // display the new address
                led_disp_set_hex(i2c_get_addr());
                ctrl_state = CTRL_WAIT_CMD;
            }
            else
            {
                led_disp_set_raw(0x0202); // --
            }
            break;
        case CTRL_WAIT_CMD:
            // WAIT_CMD - accept cut/calibrate/spacing commands
            // - receives:
            //   - UPDATE_CUT (start a cut operation) -> CTRL_DO_CUT
            //   - UPDATE_CAL (start a calibration)   -> CTRL_FIND_STOP
            //   - UPDATE_SPACING (write the spacing to EEPROM)
            // - ignores:
            //   - UPDATE_RUN
            
            // motors: stepper=off cutter=off
            stepper_enable(0);
            if (get_btn_b()) motor_set_power(DEBUG_PWR);
            else if (get_btn_a()) motor_set_power(-DEBUG_PWR);
            else motor_set_power(0);
            // status: err=0 run=0 cal=0 

            // led_disp_set_hex(i2c_get_addr());
            led_disp_set_hex(flags);

            if (flags & UPDATE_RUN)
            {
                update_flags &= ~(UPDATE_RUN);
            }
            if (flags & UPDATE_SPACING)
            {
                update_flags &= ~(UPDATE_SPACING);
                spacing = spacing_reg;
                save_eeprom_data();
            }
            if (flags & UPDATE_CAL)
            {
                update_flags = 0; // drop everything else 
                start_cal();
            }
            else if (flags & UPDATE_CUT)
            {
                start_cut();
            }
            break;
        case CTRL_DO_STEP:
            // DO_STEP - run the stepper motor
            // - receives:
            //   - UPDATE_RUN (stop the operation)
            // - ignores:
            //   - UPDATE_CUT
            //   - UPDATE_CAL
            //   - UPDATE_SPACING

            // check for general errors
            if (check_run_errors(flags))
                break;

            // check if we reached the target *normally*
            if (!stepper_running())
            {
                // move to the cut step
                _delay_ms(500);
                ctrl_state = CTRL_DO_CUT;
            }
            break;
        case CTRL_DO_CUT:
            // DO_CUT - run the cutter motor 
            // note - stepper should still be enabled here (but not moving)
            motor_set_power(CUT_PWR);

            if (check_run_errors(flags))
                break;

            if (check_enc_stall())
            {
                // back up and then error
                gp_time_last = millis(); // record start time
                ctrl_state = CTRL_STALL_REC;
                break;
            }
            
            // if the system is calibrated, 0 should be at the end stop
            if (motor_get_encoder_count() >= -CUT_STOP)
            {
                stepper_enable(0);
                motor_set_power(0);
                _delay_ms(100); // allow some rest time before returning

                status_reg &= ~(STATUS_RUN);
                ctrl_state = CTRL_WAIT_CMD;
            }
            break;
        case CTRL_DO_CUT_REV:
            // DO_CUT_REV - run the cutter motor back
            motor_set_power(-CUT_PWR);

            if (check_run_errors(flags))
                break;

            if (check_enc_stall())
            {
                do_error(ERR_CUT_STALL);
                break;
            }

            if (motor_get_encoder_count() <= -CUT_DISTANCE)
            {
                motor_set_power(0);
                _delay_ms(1000);
                // send the step command
                add_step(cut_count);

                // move the reel in
                ctrl_state = CTRL_DO_STEP;
            }
            break;
        case CTRL_STALL_REC:
            // STALL_REC - attempt to recover from a stalled cut by backing out
            motor_set_power(-CUT_PWR);

            if (check_run_errors(flags))
                break;

            if (motor_get_encoder_count() <= -CUT_DISTANCE // stop at a distance
                    || millis() - gp_time_last >= 1000     // or after a timeout if the encoder is bad
                    || check_enc_stall())                  // or if it stalled again
            {
                motor_set_power(0);
                do_error(ERR_CUT_STALL);
            }
            break;
        case CTRL_FIND_STOP:
            // FIND_STOP - seek the end stop
            motor_set_power(CAL_PWR);

            if (check_run_errors(flags))
                break;

            if (check_enc_stall() || millis() - gp_time_last > 10000)
            {
                // done
                motor_set_power(0);
                _delay_ms(300); // let the system settle
                motor_reset_encoder();
                status_reg &= ~(STATUS_CAL | STATUS_RUN);
                ctrl_flags |= CTRL_FLAG_CALIBRATED;
                ctrl_state = CTRL_WAIT_CMD;
            }
            break;
        case CTRL_ERROR:
            motor_set_power(0);
            stepper_enable(0);
            led_disp_set_hex(0xE0 | (status_reg >> 4));

            update_flags &= ~(UPDATE_RUN);

            if (update_flags & UPDATE_ERR)
            {
                update_flags &= ~(UPDATE_ERR);
                status_reg &= ~(STATUS_ERR);
                ctrl_state = CTRL_WAIT_CMD;
            }
            break;
     }
}


void do_test_sm(void)
{
    if (millis() - gp_time_last >= 100)
    {
        gp_time_last = millis();

        if (get_btn_b()) stepper_step(10);
        else if (get_btn_a()) stepper_step(-10);

        /*
        if (get_btn_b()) motor_set_power(DEBUG_PWR);
        else if (get_btn_a()) motor_set_power(-DEBUG_PWR);
        else motor_set_power(0);
        */
    }

    // led_disp_set_hex((uint8_t)motor_get_encoder_count());
    // led_disp_set_hex(i2c_get_addr());
    check_vsense();
    led_disp_set_hex(get_bcd(get_vsense_mv() / 1000));
}

int main(void)
{
    sei();

    led_disp_init();
    adc_init();
    motor_init();
    buttons_init();
    
    load_eeprom_data();

    i2c_init();
    init_timers();

    // set the motor direction so that positive power = cut direction
    motor_set_dir(CUT_DIR);

    // stepper_init() depends on the timers being started
    stepper_init();
    stepper_set_step_rate(STEP_PERIOD);
    // stepper_enable(1);
    // maybe enable the stepper here but it should hold its position without being constantly driven

    while (1)
    {
        do_final_sm(); // run the actual code
        // led_disp_set_hex(update_flags);
        // do_test_sm();
    }

    return 0;
}
