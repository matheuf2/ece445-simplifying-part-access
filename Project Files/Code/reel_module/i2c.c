#include "i2c.h"

#include "pins.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/twi.h>

#define DEFAULT_ADDR 0x7F

// what are they sending
#define S_WRITE_REG  0
#define S_WRITE_DATA 1

static volatile uint8_t negotiated = 0;

static volatile uint8_t reg_state = S_WRITE_REG;
static volatile uint8_t reg = 0;

ISR(TWI0_vect)
{
    // something happened
    // check the state
    switch (TWSR0 & TW_STATUS_MASK)
    {
        case TW_ST_SLA_ACK: // address acknowledged, start a read
            // be nice, always send data when requested (don't clear TWEA)
            // if the read continues, keep sending the same register
            // update the current register value for the next read
            TWDR0 = i2c_read_handler(reg);
            break;
        case TW_ST_DATA_ACK: // data acknowledged (master requesting more)
            // update the register again
            TWDR0 = i2c_read_handler(reg);
            break;
        case TW_ST_DATA_NACK: // data not acknowledged (master done requesting data)
            // nothing to do, return to the wait-for-address
            break;
        
        case TW_SR_SLA_ACK: // address acknowledged, start a write
            // acknowledge any data that comes thru here
            // assume that the next byte in here is a register index
            reg_state = S_WRITE_REG;
            break;
        case TW_SR_DATA_ACK: // new data available in a TWDR near you
            if (!negotiated)
            {
                TWAR0 = (TWDR0) << TWA0;
                negotiated = 1;
            }
            else
            {
                if (reg_state == S_WRITE_REG)
                {
                    reg = TWDR0;
                    reg_state = S_WRITE_DATA;
                }
                else
                {
                    i2c_write_handler(reg, TWDR0);
                    // read another register index
                    reg_state = S_WRITE_REG;
                }
            }
            break;
        case TW_SR_STOP: // stop bit received
            // make sure passthru is enabled if negotation completed
            if (negotiated)
                WRITE_PIN(I2C_OUT_PORT, I2C_OUT_PIN, 1);

            // this is called after TW_SR_DATA_ACK so the data has already been processed
            break;
    }

    TWCR0 |= (1 << TWINT); // set the interrupt flag again
}

void i2c_init(void)
{
    // initialize pins
    DDRx(I2C_OUT_PORT) |= I2C_OUT_PIN;   

    TWAR0 = (DEFAULT_ADDR) << TWA0;
    TWCR0 = (1 << TWEA) | (1 << TWEN) | (1 << TWIE);
}

uint8_t i2c_negotiated(void)
{
    return negotiated;
}

uint8_t i2c_get_addr(void)
{
    return TWAR0 >> TWA0;
}
