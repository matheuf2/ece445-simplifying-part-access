#pragma once

#include "pins.h"
#include <stdint.h>

/*
 * LED display control
 * - Controls the shift register and select outputs to print numbers on the LED display.
 * - Cycles between display outputs on a slow timer interrupt (TC1)
 */

/*
 * Initialize the pins for the LED display.
 */
static inline void led_disp_init(void)
{
    // set pin directions
    DDRx(LED_DATA_PORT)  |= LED_DATA_PIN;
    DDRx(LED_SHIFT_PORT) |= LED_SHIFT_PIN;
    DDRx(LED_STORE_PORT) |= LED_STORE_PIN;
    DDRx(LED_ENABLE_PORT) |= LED_ENABLE_PIN;

    // note: do not set both select pins ON at the same time, or the shift register
    // will self destruct
    DDRx(DISP_SEL0_PORT) |= DISP_SEL0_PIN;
    DDRx(DISP_SEL1_PORT) |= DISP_SEL1_PIN;

    // note 2: the enable port is active low, so the register is already enabled
    // however, the mosfets need a high signal to turn on so the LEDs aren't going to do anything
}

/*
 * Update the LED and toggle the selected digit.
 * To be called from the slow timer interrupt (or manually for testing).
 */
void led_disp_update(void);

/*
 * Set the raw segment values to display. The data is arranged in the following format:
 * | 15| 14| 13| 12| 11| 10| 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
 * | A1| B1| C1| D1| E1| F1| G1| x | A2| B2| C2| D2| E2| F2| G2| x |
 */
void led_disp_set_raw(uint16_t segments);

/*
 * Display a hexidecimal value on the LED display. The most significant digit will be 0
 * (not blank) if the value is less than 0x10.
 */
void led_disp_set_hex(uint8_t val);

