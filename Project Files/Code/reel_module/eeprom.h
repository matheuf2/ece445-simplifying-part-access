#pragma once

#include <stdint.h>

/*
 * EEPROM access routines
 * These functions allow saving data into persistent storage on the chip. However,
 * each write operation takes a fairly long time, so these functions should not be
 * used inside interrupts or other routines that need to maintain precise timing.
 *
 * This chip contains 1kB of EEPROM, so addresses will wrap around at 1024.
 */

/* Read a byte from the EEPROM. This is very fast (the actual read is 1 cycle).
 */
uint8_t eeprom_read_byte(uint16_t addr);

/* Write a byte to the EEPROM.
 * This operation takes 3.4 ms per byte, but it returns immediately after starting the
 * operations. The next EEPROM access will wait until this write cycle finishes.
 * Since it only starts the operation, interrupts are only disabled for a few instructions.
 */
void eeprom_write_byte(uint16_t addr, uint8_t data);
