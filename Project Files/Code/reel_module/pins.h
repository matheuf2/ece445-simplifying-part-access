#pragma once

#include <avr/io.h>

#define DDRx(p)  (*(&p - 1))
#define PINx(p)  (*(&p - 2))
#define PORTx(p) p

// Encoder A (input): PB0
#define ENC_A_PORT      PORTB
#define ENC_A_PIN       (1 << 0)
#define ENC_A_PCINT     0 // pin-change interrupt number

// Encoder B (input): PB1
#define ENC_B_PORT      PORTB
#define ENC_B_PIN       (1 << 1)
#define ENC_B_PCINT     1

// Stepper enable (output): PB2
#define STEP_EN_PORT    PORTB
#define STEP_EN_PIN     (1 << 2)

// 12V sense (input + ADC) 
#define SENSE_PORT      PORTC
#define SENSE_PIN       (1 << 0)
#define SENSE_ADC       0 // ADC mux index

// IR input (input + ADC)
#define IR_PORT         PORTC
#define IR_PIN          (1 << 1)
#define IR_ADC          1

// Stepper step (output)
#define STEP_PORT       PORTC
#define STEP_PIN        (1 << 2)

// Stepper direction (output)
#define STEP_DIR_PORT   PORTC
#define STEP_DIR_PIN    (1 << 3)

// SDA/SCL (PC4/PC5) are handled by the I2C0 peripheral

// LED data (output)
#define LED_DATA_PORT   PORTD
#define LED_DATA_PIN    (1 << 0)

// LED shift (output)
#define LED_SHIFT_PORT  PORTD
#define LED_SHIFT_PIN   (1 << 1)

// LED store (output)
#define LED_STORE_PORT  PORTD
#define LED_STORE_PIN   (1 << 2)

// I2C enable (output)
#define I2C_OUT_PORT    PORTD
#define I2C_OUT_PIN     (1 << 3)

// LED enable (output)
#define LED_ENABLE_PORT PORTD
#define LED_ENABLE_PIN  (1 << 4)

// Motor inputs can be PWM'd via OC0A / OC0B (timer 0)
// Motor In1 (output)
#define MOTOR_IN1_PORT  PORTD
#define MOTOR_IN1_PIN   (1 << 5)

// Motor In2 (output)
#define MOTOR_IN2_PORT  PORTD
#define MOTOR_IN2_PIN   (1 << 6)

// Button A (input/pull-up)
#define BUTTON_A_PORT   PORTE
#define BUTTON_A_PIN    (1 << 0)

// Button B (input/pull-up)
#define BUTTON_B_PORT   PORTE
#define BUTTON_B_PIN    (1 << 1)

// Display select 0 (output)
#define DISP_SEL0_PORT  PORTE
#define DISP_SEL0_PIN   (1 << 2)

// Display select 1 (output)
#define DISP_SEL1_PORT  PORTE
#define DISP_SEL1_PIN   (1 << 3)

// they say stupid macro hacks are the root of all evil
// but my brain woke up today and chose violence

#define WRITE_PIN(port, pin, state) do { \
        if (state) PORTx(port) |= (pin); \
        else       PORTx(port) &= ~(pin); \
    } while (0)

#define READ_PIN(port, pin) (PINx(port) & (pin))

