#include "stepper.h"

#include <avr/io.h>
#include <util/atomic.h>
#include <util/delay.h>

#include "pins.h"
#include "timing.h"

static volatile uint8_t stepper_en = 0;
static volatile uint8_t stepper_run = 0;
static volatile int32_t stepper_position = 0;
static volatile int32_t stepper_target = 0;
static uint32_t stepper_rate = 0;

void timer3_handler(void)
{
    if (!stepper_en)
    {
        stepper_run = 0;
        return; // do nothing if the stepper is disabled
    }

    int32_t pos = stepper_position;
    int32_t tgt = stepper_target;

    if (pos == tgt)
    {
        stepper_run = 0;
        return; // do nothing if we are at our target
    }

    uint8_t reverse = (tgt < pos);
    
    WRITE_PIN(STEP_DIR_PORT, STEP_DIR_PIN, reverse);
    _delay_us(2);
    WRITE_PIN(STEP_PORT, STEP_PIN, 1);
    _delay_us(2);
    WRITE_PIN(STEP_PORT, STEP_PIN, 0);
    stepper_position += (reverse) ? -1 : 1;
}

void stepper_init(void)
{
    // set outputs
    WRITE_PIN(STEP_EN_PORT, STEP_EN_PIN, 1); // enable is active-low, set it high
    DDRx(STEP_EN_PORT)  |= STEP_EN_PIN;
    DDRx(STEP_PORT)     |= STEP_PIN;
    DDRx(STEP_DIR_PORT) |= STEP_DIR_PIN;

    stepper_set_step_rate(10000); // set timer3 to 10000 us
}

void stepper_set_target(int32_t pos)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        stepper_target = pos;
        stepper_run = 1;
    }
}

void stepper_step(int32_t delta)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        stepper_target += delta;
        stepper_run = 1;
    }
}

void stepper_stop(void)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        stepper_target = stepper_position;
    }
}

int32_t stepper_get_target(void)
{
    int32_t target;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        target = stepper_target;
    }
    return target;
}

int32_t stepper_get_position(void)
{
    int32_t pos;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        pos = stepper_position;
    }
    return pos;
}

void stepper_set_step_rate(uint32_t rate)
{
    if (rate < 200) rate = 200;
    set_timer3_rate(rate);
    stepper_rate = rate;
}

uint32_t stepper_get_step_rate(void)
{
    return stepper_rate;
}

uint8_t stepper_running(void)
{
    return stepper_run;
}

uint8_t stepper_enabled(void)
{
    return stepper_en;
}

void stepper_enable(uint8_t enable)
{
    WRITE_PIN(STEP_EN_PORT, STEP_EN_PIN, !enable);
    stepper_en = enable;
}
