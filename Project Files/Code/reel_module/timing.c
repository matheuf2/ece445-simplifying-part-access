#include "timing.h"

static volatile uint32_t millis_count = 0;

////////////////////////////////
// Interrupt handlers
ISR(TIMER1_COMPA_vect)
{
    // interrupt on timer 1 (1000Hz interval)
    // increment the millisecond counter
    millis_count++;
    
    timer1_handler();
}

ISR(TIMER3_COMPA_vect)
{
    timer3_handler();
}

static inline void init_timer1(void)
{
    // set up timer1 to be a 1000Hz interrupt timer
    // input clock = 12000000 Hz
    // need to divide by 12000
    // prescaler options:
    // - 1/8    => remaining division = 1500
    // - 1/64   => remaining division = 1875
    // - 1/256  => remaining division = 468.75
    // since we can't do fractional division (and we can use the timer to divide by up to 65535)
    // we choose the 1/8 option (CS1 = 011)
    //
    // next, we have to set up the timer to count to 1499 so we have 1500 clocks per
    // interrupt. This can be done by enabling CTC mode (WGM1 = 0100) and setting OCR1A.
    const int freq = 1000;
    
    const uint16_t period = (uint16_t)((F_CPU / (8 * freq)) - 1);
    // atomically update 16-bit registers since they use a shared temporary byte that can get
    // corrupted if an interrupt happens
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        OCR1A = period;
    }
    // set the timer configuration registers
    // TCCR1A:
    // - COM1A = 00 (OC1A output disabled)
    // - COM1B = 00 (OC1B output disabled)
    // - WGM1[1:0] = 00 (WGM1 = 0100)
    // => 00 00 00 00 = 0x00
    TCCR1A = 0;
    // TCCR1B:
    // - ICNC1 = 0 (no input capture, no noise canceling needed)
    // - ICES1 = 0 (again, no input capture)
    // - WGM1[3:2] = 01 (WGM1 = 0100)
    // - CS1 = 010 (CLK / 8)
    // note this connects a clock to the timer, starting it
    TCCR1B = (1 << WGM12) | (1 << CS11); 

    // enable interrupts when the timer reaches OCR1A
    // TIMSK1
    // - ICIE1 = 0 (no input compare)
    // - OCIE1B = 0
    // - OCIE1A = 1 (interrupt on OC1A)
    // - TOIE1 = 0 (no interrupt on overflow)
    TIMSK1 = (1 << OCIE1A);
}

static inline void init_timer0(void)
{
    // set up timer 0 as an 8-bit PWM timer for the motor output
    // note: the frequency MUST be greater than 20kHz, otherwise the pulses will be
    // VERY audible through the motor windings.

    // fortunately, we find that with an 8-bit counter clocked at 12MHz, the PWM
    // frequency is 46.875kHz, well outside the audible range.

    // COM0A = COM0B = 10 (clear on compare match)
    // WGM = 011 (fast PWM)
    TCCR0A = (1 << COM0A1) | (1 << COM0B1) | (1 << WGM01) | (1 << WGM00);
    // CS0 = 001 (no prescaling)
    TCCR0B = (1 << CS00);

    // PWM is now enabled! However, the outputs are disabled since the OCRs are 0.
}

static inline void init_timer3(void)
{
    // similar to timer2, we need a periodic interval timer
    // this one needs to have an adjustable rate though
    // as such, we are going to have the clock source disabled until we know which
    // prescaler to use.
    
    // - COM3A = 0
    // - COM3B = 0
    // - WGM3 = 0100 ([1:0] = 00)
    TCCR3A = 0;
    // - ICNC3 = 0
    // - ICES3 = 0
    // - WGM3 = 0100 ([3:2] = 01)
    // - CS1 = 000 (no clock)
    TCCR3B = (1 << WGM32);

    // enable interrupts
    // - ICIE3 = 0
    // - OCIE3B = 0
    // - OCIE3A = 1 (interrupt when the top is reached)
    // - TOIE3 = 0
    TIMSK3 = (1 << OCIE3A);

    // to update the period, we calculate values for OCR3A and the prescaler
}

void init_timers(void)
{
    init_timer0();
    init_timer1();
    init_timer3();
}

uint32_t millis(void)
{
    uint32_t millis;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        millis = millis_count;
    }
    return millis;
}

void set_timer3_rate(uint32_t rate)
{
    const uint32_t clocks_per_us = F_CPU / 1000000;

    // valid dividers:
    // 1 = /1
    // 2 = /8
    // 3 = /64
    // 4 = /256
    // 5 = /1024
    // note that each successive division is 8x the last
    uint32_t ticks = rate * clocks_per_us;
    uint8_t div = 1;
    for (; div <= 5; ++div)
    {
        if (ticks <= 65536) break;
        else ticks /= 8;
    }

    if (div == 6)
    {
        // clamp to the maximum
        div = 5;
        ticks = 65536;
    }
    
    // turn off the clock
    TCCR3B &= ~((1 << CS32) | (1 << CS31) | (1 << CS30));

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        // atomically update the OCR
        OCR3A = (uint16_t)(ticks - 1);
    }

    // enable the clock again with the new divider
    TCCR3B |= div;
}
