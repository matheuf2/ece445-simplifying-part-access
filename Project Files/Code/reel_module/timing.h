#pragma once

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

// initialize any timers for PWM and interval timing
void init_timers(void);

// get the current number of milliseconds since init_timers() was called
uint32_t millis(void);

// timer1 handler (to be defined in the main function)
extern void timer1_handler(void);
// timer3 handler (to be defined in the stepper code)
extern void timer3_handler(void);

// PWM functions
static inline void pwm_set_a(uint8_t v)
{
    // directly set the output compare registers
    OCR0A = v;
}

static inline void pwm_set_b(uint8_t v)
{
    OCR0B = v;
}

// set the timer3 rate in us per tick
void set_timer3_rate(uint32_t rate);
