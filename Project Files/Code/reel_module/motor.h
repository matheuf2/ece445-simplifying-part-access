#pragma once

#include <stdint.h>
#include <avr/io.h>

// motor_set_dir() options
#define M_FORWARD 0
#define M_REVERSE 1

// motor_set_brake_mode() options
#define M_BRAKE   0
#define M_FLOAT   1

// initialize pins for the encoder
void motor_init(void);

// apply power to the motor; negative values reverse direction
// range is clipped to -255 to +255
// there is (currently) no closed-loop control here
void motor_set_power(int16_t power);

void motor_set_dir(uint8_t dir);
// whether to brake or coast (float) at 0 power
void motor_set_brake_mode(uint8_t mode);

int32_t motor_get_encoder_count(void);
void motor_reset_encoder(void);
