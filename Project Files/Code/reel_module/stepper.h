#pragma once

#include <stdint.h>

// initialize the stepper to a disabled state with a 10ms per step rate
void stepper_init(void);

// set the target position. 
void stepper_set_target(int32_t pos);

// add to the target position
void stepper_step(int32_t delta);

// stop the current move by setting the target to the position.
void stepper_stop(void);

int32_t stepper_get_target(void);
int32_t stepper_get_position(void);

// set the step rate (us per step)
// this rate is clamped at a minimum of 200us per step and a maximum of (65536 * 1024)/12 us (~5.6s) per step
// different prescalers are used for different rates, so the resolution is variable:
// * 200     - 5461    us : no prescaler   (increment = 1 us)
// * 5462    - 43691   us : divide by 8    (increment = 3 us)
// * 43692   - 349530  us : divide by 64   (increment = 5 us)
// * 349531  - 1398122 us : divide by 256  (increment = 21 us)
// * 1398123 - 5592405 us : divide by 1024 (increment = 85 us)
void stepper_set_step_rate(uint32_t rate);
uint32_t stepper_get_step_rate(void);

// return 1 if the target is not equal to the position and the stepper is not disabled
uint8_t stepper_running(void);

// return 1 if the stepper is enabled
uint8_t stepper_enabled(void);

// enable/disable the stepper motor
void stepper_enable(uint8_t enable);

