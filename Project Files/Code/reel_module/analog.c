#include "analog.h"
#include "pins.h"

void adc_init(void)
{
    // - enable ADC (ADEN)
    // - set prescale to 111 (1/128 -> 93.75kHz)
    ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
    // - set reference to 01 (AVcc)
    ADMUX = (1 << REFS0);
}

uint16_t analog_read(uint8_t adc_idx)
{
    // set the multiplexer to the specified index
    ADMUX &= ~(0xF);
    ADMUX |= (adc_idx & 0xF);

    ADCSRA |= (1 << ADSC); // start conversion
    while (ADCSRA & (1 << ADSC)); // busy-wait until the result arrives
    
    return ADC;
}

// application specific
static uint16_t vsense_last = 0;

// at 3.3V (reading = 1024), the divider input will be
// r = 12 / (12 + 33)
// 3.3 / r = 12.375 (volts)
#define VSENSE_MV_MAX 12375

#define VSENSE_THRESH_MV 6000 // 6 V

// convert to raw ADC reading
#define VSENSE_THRESH (((uint32_t)VSENSE_THRESH_MV * 1024) / VSENSE_MV_MAX)

// arbitrary threshold for the IR port
#define IR_THRESH 512

void check_vsense(void)
{
    vsense_last = analog_read(SENSE_ADC);
}

uint16_t get_vsense_mv(void)
{
    // process as a 32 bit integer to avoid overflow
    uint32_t vs = vsense_last;
    vs = (vs * VSENSE_MV_MAX) / 1024;
    return (uint16_t)vs;
}

uint8_t vsense_ok(void)
{
    return vsense_last > VSENSE_THRESH;
}

uint16_t get_ir_raw(void)
{
    return analog_read(IR_ADC);
}

uint8_t ir_state(void)
{
    return get_ir_raw() > IR_THRESH;
}
