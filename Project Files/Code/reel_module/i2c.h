#pragma once

#include <stdint.h>

void i2c_init(void);

// to implement in the main function
// runs in the i2c interrupt handler
extern void i2c_write_handler(uint8_t reg, uint8_t val);
extern uint8_t i2c_read_handler(uint8_t reg);

uint8_t i2c_negotiated(void);
uint8_t i2c_get_addr(void);
