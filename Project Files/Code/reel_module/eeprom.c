#include "eeprom.h"

#include <avr/io.h>
#include <util/atomic.h>

uint8_t eeprom_read_byte(uint16_t addr)
{
    while (EECR & (1 << EEPE)); // wait until the last write completes
    EEAR = addr;
    
    // perform a read
    EECR |= (1 << EERE);
    return EEDR;
}

void eeprom_write_byte(uint16_t addr, uint8_t data)
{
    while (EECR & (1 << EEPE));
    EEAR = addr;
    EEDR = data;
    
    // clear interrupts because we need to set both of these in very short succession
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        EECR |= (1 << EEMPE); // enable writing
        EECR |= (1 << EEPE);  // start the write operation
    }
}
