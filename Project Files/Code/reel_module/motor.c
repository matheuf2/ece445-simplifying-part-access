#include "motor.h"

#include <avr/interrupt.h>
#include <util/atomic.h>
#include "pins.h"
#include "timing.h"

#define S_DIR_BIT   0
#define S_FLOAT_BIT 1

static volatile int32_t encoder_count = 0;
static volatile uint8_t last_enc_state = 0;
static uint8_t motor_state = 0;

ISR(PCINT0_vect)
{
    // something changed on one of the two encoder pins
    uint8_t new_enc_state = 0;
    if (READ_PIN(ENC_A_PORT, ENC_A_PIN)) new_enc_state |= 2;
    if (READ_PIN(ENC_B_PORT, ENC_B_PIN)) new_enc_state |= 1;

    int8_t dir = 0;
    switch (last_enc_state)
    {
        case 0:
            if      (new_enc_state == 1) dir = 1;
            else if (new_enc_state == 2) dir = -1;
            break;
        case 1:
            if      (new_enc_state == 3) dir = 1;
            else if (new_enc_state == 0) dir = -1;
            break;
        case 2:
            if      (new_enc_state == 0) dir = 1;
            else if (new_enc_state == 3) dir = -1;
            break;
        case 3:
            if      (new_enc_state == 2) dir = 1;
            else if (new_enc_state == 1) dir = -1;
            break;
    }
    dir = -dir; // fix the encoder count direction
    if (motor_state & (1 << S_DIR_BIT))
        dir = -dir;
    encoder_count += dir;
    last_enc_state = new_enc_state;
}

void motor_init(void)
{
    // input pins do not need their DDRs set (they are inputs already)
    // output pins do need to be set to output
    DDRx(MOTOR_IN1_PORT) |= MOTOR_IN1_PIN;
    DDRx(MOTOR_IN2_PORT) |= MOTOR_IN2_PIN;

    // enable pull-up resistors on the encoder pins so that the interrupt
    // system doesn't get flooded if the motor is disconnected
    WRITE_PIN(ENC_A_PORT, ENC_A_PIN, 1);
    WRITE_PIN(ENC_B_PORT, ENC_B_PIN, 1);

    // set up pin change interrupts for the encoder
    // both pins are on PCINT block 0 (interrupt 0 and 1 specifically)
    PCMSK0 |= (1 << PCINT0) | (1 << PCINT1);
    PCICR |= (1 << PCIE0);
}

void motor_set_power(int16_t power)
{
    if (power == 0)
    {
        if (motor_state & (1 << S_FLOAT_BIT))
        {
            // float
            pwm_set_a(0);
            pwm_set_b(0);
        }
        else
        {
            // brake
            pwm_set_a(255);
            pwm_set_b(255);
        }
    }
    else
    {
        uint8_t dir = motor_state & (1 << S_DIR_BIT);
        if (power < 0)
        {
            power = -power;
            dir = !dir;
        }
        if (power > 255) power = 255;

        if (dir)
        {
            // reverse
            // IN1 0 (on) -> 1 (off)
            // IN2 always 1
            // -> toggle between off/brake
            pwm_set_a(255 - (uint8_t)power);
            pwm_set_b(255);
        }
        else
        {
            // forward
            // IN1 always 1
            // IN2 0 (on) -> 1 (off)
            pwm_set_a(255);
            pwm_set_b(255 - (uint8_t)power);
        }
    }
}

void motor_set_dir(uint8_t dir)
{
    if (dir) motor_state |= (1 << S_DIR_BIT);
    else     motor_state &= ~(1 << S_DIR_BIT);
}

void motor_set_brake_mode(uint8_t mode)
{
    if (mode) motor_state |= (1 << S_FLOAT_BIT);
    else      motor_state &= ~(1 << S_FLOAT_BIT);
}

int32_t motor_get_encoder_count(void)
{
    int32_t enc;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        enc = encoder_count;
    }
    return enc;
}

void motor_reset_encoder(void)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        encoder_count = 0;
    }
}
