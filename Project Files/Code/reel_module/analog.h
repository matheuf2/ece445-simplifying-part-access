#pragma once

#include <avr/io.h>

void adc_init(void);

// read from an ADC pin
// this takes a relatively long time (130 to 250 us)
// so it should not be called from the timer interrupt
uint16_t analog_read(uint8_t adc_idx);

// analog stuff for this specific board

// Check the 12V sense pin; run this every 500ms
void check_vsense(void);

// Get the sense voltage, converted to mV
uint16_t get_vsense_mv(void);

// Return 1 if the sense voltage is near the expected 12 V and 0 otherwise. 
uint8_t  vsense_ok(void);

// Get the raw IR sensor reading (0-1023)
uint16_t get_ir_raw(void);

// Get a boolean value representing the current state of the IR sensor
uint8_t ir_state(void);

