#include "led_disp.h"
#include <util/atomic.h>

// write from main loop, read from interrupt
static volatile uint16_t segments_curr = 0;

// read/write from interrupt only
static uint8_t digit_sel = 0; // 0 or 1

void led_disp_update(void)
{
    uint16_t seg = segments_curr;

    digit_sel = !digit_sel;
    
    if (digit_sel == 0)
    {
        // shift out the high byte (digit 0)
        seg = seg >> 8;
    }

    for (uint8_t i = 0; i < 8; i++)
    {
        // since this loop is at least 2 instructions (< 6MHz output)
        // and the shift register can operate at up to a 30MHz shift clock,
        // we do not need to put any delays in this loop!
        //
        // We could also use the SPI port to maximize throughput, but it doesn't really matter
        WRITE_PIN(LED_DATA_PORT, LED_DATA_PIN, (seg & 0x01));
        WRITE_PIN(LED_SHIFT_PORT, LED_SHIFT_PIN, 1);
        asm volatile("nop\n\t"); // add a nop so the high pulse isn't too short
        WRITE_PIN(LED_SHIFT_PORT, LED_SHIFT_PIN, 0);
        seg >>= 1;
    }

    // update the selected digit
    WRITE_PIN(DISP_SEL0_PORT, DISP_SEL0_PIN, 0);
    WRITE_PIN(DISP_SEL1_PORT, DISP_SEL1_PIN, 0);
    // update the register outputs
    WRITE_PIN(LED_STORE_PORT, LED_STORE_PIN, 1);
    asm volatile("nop\n\t");
    WRITE_PIN(LED_STORE_PORT, LED_STORE_PIN, 0);

    WRITE_PIN(DISP_SEL0_PORT, DISP_SEL0_PIN, !digit_sel);
    WRITE_PIN(DISP_SEL1_PORT, DISP_SEL1_PIN, digit_sel);
}

void led_disp_set_raw(uint16_t segments)
{
    // set the 16-bit value (the 8 bit processor uses 2 writes to do this)
    // this can be done non-atomically, if a read happens in between, it will update
    // to the correct value very quickly.

    // using an atomic block here might accidentally clog the interrupt system if the
    // display was updated in a tight loop.
    segments_curr = segments;
}

static const uint8_t hex_digits[16] = {
    0xFC, 0x60, 0xDA, 0xF2, 0x66, 0xB6, 0xBE, 0xE0,
    0xFE, 0xF6, 0xEE, 0x3E, 0x9C, 0x7A, 0x9E, 0x8E
};

void led_disp_set_hex(uint8_t val)
{
    uint8_t seg[2];
    for (uint8_t i = 0; i < 2; i++)
    {
        seg[i] = hex_digits[val & 0xF];
        val >>= 4;
    }
    led_disp_set_raw(seg[0] | (seg[1] << 8));
}
