# ECE445 Simplifying Part Access

The goal of this project is to make it simpler to count and dispense large numbers of small SMD components used in ECE445. This allows for streamlining the processes the class utilizes.

This will contain the notebooks used throughout this project.

Team Members:
* Matheu Fletcher
* Aidan Yaklin
* Tejas Aditya
