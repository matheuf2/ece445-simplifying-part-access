# Matheu Worklog

[[_TOC_]]

## Design Documentation meeting 9/27/2023

Met with other group mates to reconfirm parts, specifically the stepper and gear motor needed for the project.
Decided upon two specific motors and ordered them.

## Design Documentation Meeting more 9/28/2023
Met more to work on the R&V Tables for the various components in the documentation. Worked on adding information surrounding the cutter.
Picked up the reel and started taking measurements to try to design the case and spacing of the various components

##  Control Board Design 10/01/2023
Met over Discord with Tejas to work on designing and implementing the circuit design for the control box, including esp32, screen, encoder, and numpad.
Primarily worked on understanding whether or not to use the IO/MUX or GPIO Matrix pin configuration on the esp32, and what the implications of either would be.

## Motor attached Research and design 10/02/2023
While we wait for parts to get in for us to be able to actually test the motors, I have started working to design the pieces for the cutting blade, namely connecting the blade to the motor and designing parts for holding it.

## Meeting with TA  10/09/2023
I met alone with our TA as Aidan was home sick and Tejas was not back yet from his trip. We mostly discussed where we were with the PCB, otherwise just making sure we get parts ordered and finish getting other parts designed for the machine shop.

## Mechanical Design 10/13/2023
I completely got behind working on the mecahnical design portion since we did not have parts in yet, but I finally started working on designing the core box layout of the components, taking our general design and adding more specific elements.
![General Layout](image.png)

This is the core design.

Top left consisting of the opening alongside the set of rollers with the bottom one connected to the motor. The top one we want springloaded to keep enough pressure on the tape but was not sure how to do it. The little box inside is just to size out how the PCB will fit inside the overall piece.

Additionally the big donut looking thing is to showcase how both the 7inch (inner diameter) and 13 inch (outer diameter) reels will both fit, while the rod and the little endcaps show how we will handle clicking in and out the spindle to change out the spools. The exact angle and designs will likely need a little revision.

This design process started later than it should have and this is much rougher than the design really should be.
Currently the cutter mechanism still needs added and the way that the top roller should be spring loaded so we can keep pressure on it.

## Meeting with Gregg Bennett 10/13/2023
I went and met with Gregg Bennett in the machine shop again to discuss what I had of the design as it was supposed to be the last day for revisions. I went in knowing that the design was not finalized enough to meet exact requirements but went to get his advice and show what we had. He concurred that it was not enough for what they needed yet, so he asked me to get parts finalized to get exact dimensions and stuff figured out over the weekend and come meet with him again next week to make sure to get the ball rolling on this.
He also specified that he makes no promises that they will be able to get both boxes done in the timeline as things are already running short.

As such, I suspect I will likely need to handle printing and building the control box myself, and might also have to similarly do the same for one of the reel modules if they are not able to get both boxes made. So that even further requires me to get things refined.

Also discussions sounds like for the blades that they will actually cut and bevel a piece of steel for it rather than us using standard razard blades like we previously considered.

So currently though I need to get details of mounts more finetuned, get cutter portion added and refined.

Additionally, he also took me to get a couple motors and a 13" spool to borrow to mess with for more testing, which should prove beneficial.
But the big take away as always is we have to get everything ordered today so we can start actually working on it!

## Ordering 10/18/2023
After quick discussion with the group and knowing we needed to, finally got parts actually ordered. Including Digikey order and both our stepper motors off amazon, along with another gear motor for the cutter.
100% behind and need to get the CAD design fleshed out in the next couple days as if we wanted anything done, Gregg Bennett needs us to get it to him this week.

## Design Continued 10/19/2023
Continued working on the cad design, trying add the cutting motor, fix issues with how the bodies in the model were previously designed. Using the dimensions of the motor I have. After a couple minor tweaks such as making a larger opening for any wires or other components, I ended up with the following.

![Right side enclosed](image-1.png)
![Left side enlosed](image-2.png)
![Left side open](image-3.png)

Even with these changes, I am not 100% happy, but am hoping it is enough to take to Gregg in the machine shop to get the ball rolling. For example, we still need to add the kill switch to the front for the cutting motor for added safety.

Additionally, to account for the discussion that the machine shop may not be able to make two boxes for us in time, I made a really rough sketch on my ipad of how we could approach making the second box an open concept version of it, as trying to 3d print a box of the full size we need would be hard to do within the time contstraints. Such an approach also shows a wider versatility of how the design could be implemented and built.

The resulting design simply looks something like this, and actually needs  modeled out further in CAD. 
![Open version sketch](image-4.png)
Another point that would need changed is enclosing the cutter portion of the open design to reduce possible injury, while also adding the necessary kill switch.

Overall, I am glad to be making process on the designs, but I am sure they will both need furhter revisions to other little pieces I have not thought of, due to various components not being implemented yet.

## Meeting with Gregg Bennett 10/19/2023
Took these designs that had just been finalized and discussed and went and met with Gregg Bennett again to get information to the machine shop. He confirmed that there was little chance they would get a second one, but agreed with the idea of doing one of the boxes open, and said the first one might also end up needing to be.
I left our parts we had so far with him, and he asked for the design file, and a picture of just some of the core measurements of the design as seen below:

![Reel module with dimensions](<Reel Module Views.png>)

As such, this gave a baseline to tell we were on the right track. He did warn us to expect things to change and to get the other motors to him as soon as we can.

I still need to work on redesigning it, taking the existing components and redesigning it in cad to fit the aforementioned open design seen above.

## More CAD work and some 3D printing 10/31/2023
Now that I am back from my conference and have caught up on exams, I continued work on the the CAD design, specifically the open box version as that seems the most likely version for us to be able to easily build.
The first detail I need was figuring out the posts to use in order to hold the reel, what I ended up with was this:
![Reel Holder post piece](image-5.png)

This is simply the post design with the corresponding holes for a rod to go through it, and a couple holes at the bottom for screwing it to the board we will use as the base for the open box design.
So with that initial design done, I figured out how to design the rod that goes across it, for this, I simply decided on a cylinder, with one end being slightly wider in diameter so it will not fit through the holes, and the other end with another cylind cut out across for it, allowing us to put a cotter pin through it, to make sure that it can not slide out once it is in place.

I then got to work printing out the initial designs.
![Initial prints](image-6.png)
This also included a 3D printed Cotter pin design I scaled up from Sillybutts lever action nerf blaster called the SLAB, that I used for testing. The file can be found in the repository at https://github.com/Sillybutts/SLAB.
Upon these prints, I quickly realized that I made the pin much larger than needed, resulting in modifications to shorten it, leaving me with the second version 445 Rod Short that is uploaded here. I am still in the process of adjusting this as needed and figuring out other logistics for motor and cutter mounting in this configuration.

## Even more CAD work 10/31/2023 into 11/1/2023
Did some more work, modeling the mount for the main stepper motor roughly, and the couple Rollers, very basicly, resulting in : 
![Motor mount and rollers](image-7.png)

## Meeting with Skee G Aldrich 11/1/2023
I recieved an email from Skee in the machine shop, as he was starting to work on our project and wanted to meet with someone to discuss the design. As such, I went and met with him in the morning, and for the most part we were on the same page. THe main comments he made was that rather than spring loading the top roller, make the bottom roller like neoprene or something softer, but have the entirely motor have a little bit of play up and down to create the pressure we need. Similarly, he was just discussing having other rollers and that before that to help guide the tape, as the reel begins to reduce in diameter as components are pulled off. Also putting the spool on a small bearing to remove resistance, as he was concerned that especially when the reel gets smaller, the motor may not have enough instantaneous torque to continue pulling from the reel.
Additionally, I am uncertain if we will end up with the full 50mm width we needed to meet high level requirements due to concerns he had about the fact the blade will have to be super thin to work for us, so the longer it is, the quicker and easier something could go wrong.
He said we were mostly on the same page and that he would work on it some more and reach out when he had some more planned out on it.

Overall still seemed to be in a mostly good place, although it makes me question if the open mostly 3D printed will end well or not.

## Initial Box work 11/08/2023
I started work on the enclosure for the main control board pcb, and related components, only to realize that I was approaching it a bad way with regards to hount to mount the components. As such, I wiped out the design and noted ideas of how to do redo it, mostly making use of heat set inserts to mount the pcb and screw together the top and bottom panels. Additionally, I need to make a mount, and a terminal cover for the main power supply.

## Meeting with Skee G Aldrich 11/09/2023
After the initial TA meeting the day before, the same day we recieved a request from Skee again to meet to discuss how  we were handling part cutting. Tejas was available so he went to discuss at that point, but he asked if I could also stop by when I had time today, so I stopped by.
Skee primarily was showing off what they had got milled and built, which looked really good, his main concern was whether our gear motor had enough torque to cut through the tape. As such that would be something to see. Overall everything looked good, and I agreed, so hopefully we will have the core components done.

Also after that discussion and the previous discussion with the TA, I realized I should design another box, even if it's just motors and PCB, so that we have the ability to show that we can drive both, even if both systems are not fully built to cut.

## CAD work for Base controller Box 11/09/2023
Tonight I spent some time working on the box for the amin control board. I utilized the dimensions we knew for the pcb, making standoffs for it, and importing existing models for the screen and rotary encoder. I had to black box design the numpad, as the measurements given on their datasheet was not sufficient to properly recreate the keypad with it's mounting holes or thickness known.
Additionally, I designed it such that for the standoffs for the pcb and the lid of the box, that it makes use of m3 heat set inserts, that I will then be able to screw into, thus allow for a better secure fit of the printed components.
It was also necessary to design a Knob for our rotary encoder, which was done by designing the knob on top of the encoder, and then subtracting the encoder from the design.

The resulting pieces look like what is seen below:
![Main control box, alongside its lid and knob](image-8.png)

I asked the others if I can get an extra of each of the PCB's so that I can confirm the wholes line up without taking away their built boards.
Beyond that, waiting to confirm if the LCD added is correct before printing, as we have been struggling with which of the two screens we have to use.

Additionally, I still need to design a mount for the powersupply to go alongside that cover, along with getting and conecting  the proper wall power connector for the terminals of the power supply.

## Continued main box 11/10/2023
Continued working on the main box, printed it, then found I was not happy with the design. Namely I realized that I had not left proper space before the opening to account for the angle of cables coming from the height of the connectors connected to the PCB. As such, I opted for the simple fix of flipping the hole to the other side of the box, which has more space between the pcb and the wall. Additionally, I wanted to add a proper mount for the power supply, so I took measurements from an existing mount and used that to create a bracket attached to the box. Finally I also determined to make it look more professional, rather than leave that wire gap entirely open, that I should create a sort of grommet similar to those used on computers out of a flexible tpu filament to fill in the space.
The resulting design of the box looks like this, and is in the process of being printed.
![Updated box design with power supply mount](image-9.png)

## More cad Work 11/12/23
Continued working on designing various pieces for the box and refining it. The next thing i designed was a cover for the terminals of the power supply and printed it out.
I then took the parts I had printed so far into the lab to make sure everything would fit. I was able to fit the power supply and PCB into place, but realized the cover was designed and sized slightly wrong, due to my misinterpretation of the provided schematics at https://mm.digikey.com/Volume0/opasdata/d220001/medias/docus/2372/RD-85-SPEC-OLD_Ds.pdf . As a result, I know i need to redesign it.
Additionally, while I went in to test, i took calipers to confirm the dimensions of the other screen we opted to use, as it was supposedly a more generic size, but I wanted to make sure before printing anything. I confirmed that it matched and I could use the normal size for such an LCD found here. https://cdn.sparkfun.com/datasheets/LCD/Monochrome/ADM1602K-FS-RGB-FBW.pdf.

Lastly given I had no reference, it was still necessary to design a proper version of the keypad since none existed. So I took all the various measurements.
The main bottom portion of the body being 51 by 64.03 by 3.75mm with a 36.4 by 5.76 pcb jutting out at the bottom. The raised numpad section was 56.82 by 46mm, and is also roughly 3.75mm thick. Finally the holes are about 2mm in diameter.

The present design looks something like
![Lid design](image-10.png)

The left section is the lid with the modifications made for the screen and numpad, while the right is my rough model of the numpad from the dimension I had, which I subtracted from the lid to get the present shape. While it is printing, I am working on figuring out the way to fix the power cover, which also requires the addition of a place for a c14 inlet piece, as it is needed to get power in on the terminals to the power supply.
After which, both will be tested, and the main design work left will be creating a box for the reel module to showcase that we have the pieces for the second one, even if it doesn't fully work.

Printing the lid fit, I simply had to clean up some of the holes for the screws, then I simply added the screws I had that corresponded to the various parts so they would be there when we start actual assembly, the result looks like:

![Fully enclosed box with screws and no components yet](image-11.png)

Additionally, I reprinted the knobs at 110% and 115% in the hopes that either of those will fit. In the same print, I also attempted to print the newly simplified cover for the terminals, but I made the sides too thin, and I broke it while taking it off the build plate, made the walls thicker and am reprinting. Will see if the newly printed part works later.

## Test fitting 11/14/23
Was able to come in and test fit the various pieces. I found that the keypad fit absolutely perfectly (as can be seen below, although yes, it needs rotated 180 degrees)

![Keypad Test fit](image-12.png)

On the other hand, I found that the screen was wider than I saw it supposed to be. The screw holes appeared to line up almost precisely where I intended, but the screen itself was about 71mm wide when I measured with calipers and it appears that the hole was only 65mm wide. As such, I need to modify and update the hole for the screen, adding additional tolerances.
Finally, I found that the hole for the encoder worked, except for the little nub that can be seen on top of the body in the image below. As a result of this nub, the encoder would not sit flush enough, which does not give room for the nut to attach to hold it in place.

![Rotary encoder test fit](image-13.png)

I measured the size of this nub to be aproximately 3mm by 2mm by 2mm, and thus will attempt to modify the core design to acomodate for it.
Additionally, the newly printed knobs I printed easily fit over the post for it, although extremely losely to where they would not stay. To fix, I likely will reprint it scale down to probably 106% and see if that fits.
Finally, for the power supply cover, I made it too deep, that it didn't have a proper hold to stay on the terminals that I did not like, so planning on shortening it and making modifications to give it a better fitment.

Still a lot of work to be done, I need to additionally get a box made for the other reel module so we can showcase that the pieces function even if not fully together.
I should also be better participating in the code and other portions of the project.

## Checked in on machine Shop 11/14/23
Tejas and I stopped in on the machine shop as Skee thought things would have been done by late the previous Friday or Monday, and it was Tuesday. After talking to Gregg, we found out he was out, and as such, we are hoping to finally have it where we can test in the next couple days. The state of the project currently looks like:

![Current extent of project located in the machine shop](image-14.png)

Tejas and I also discussed all the coding that was left, mostly tieing everything together, alongside the main core feeding and cutting loop. The most unnerving part is getting both sides of the IC2 coding figured out and figure out how we handle connecting multiple boards together, as we are not sure we made it correctly to have the plugs.

Additionally, earlier in the week Tejas had stepped by as Skee had expressed concern the previous week that the gear motor we have for the cutter may not be powerful enough, so after discussion, we ordered a new one, and Tejas had it, and was dropping it off.

## Adjusted Lid 11/14/23
Adjusted the design, adding additional width for the screen so it will hopefully fit, also added .2 to .3 mm of leway on each hole depending if it was a 2mm or 3mm hole, so that the screws will more easily go through them without having to bite into the plastic.
Additionally at the request of Tejas, I modified the Lid, adding 10mm of height so that we have more height in the box for them to play with for when things are wired up and enclosed in the box.
The newly printed lid fits, I simply moved from using 12mm long screws for the edges to 20mm ones to account for the new height.
Hopefully the parts will fit, but I will have to check more later.
Similarly, I also printed off new knobs to hopefully find one that properly fits. I printed them at 104,106 and 108%. If none of them fits, worst case I am considering taking like the teflon tape plumbers use, wrapping it around the spindle of the encoder and then putting the knob over that to help keep it in place.

Still need to make the box for the secondary reel module. Hopefully everything works, and need to help with whatever is asked.

## Testing and work 11/15/23
Came in around lunch and checked in with the machine shop with Tejas, Skee was almost done with the box and needed roughly another hour. Went upstairs and checked to see if the parts fit in the new lid. We found that the screen fit, the edges were a little lose though, and the keypad would not fit due to a hole that accidentally got filled in. I then left to go to a couple classes, and came back, and helped cut and crimp ends on cables, while Aiden worked on terminals and Tejas coded.
I also realized what was left that needed designed regarding the reel box, and the spacers that are needed in general for the reel board. So that is in the works, while I fix and reprint the lid.

## Cleaning up wires and modeling 11/16/23
Came in and spent the morning shortening the wires for the screen and everything, only to have a concern that when I plugged it in that it wasn't displaying the correct thing. It turned out to simply be when i restreipped a wire, I pulled slightly too hard and it pulle the other end from it's pin, so it needed put back in its spot. We contemplated whether or not to use hot glue to fill it to prevent it from coming loose.
I also brough in the large mount for the reel board, which is about 2 inch high standoffs with m3 heat inserts to hold the board and account for the components on the underside of the board. Additionally, the base of said piece has four quarter inch holes in the corner for using quarter inch diameter wood screws to attach it to the overall box.

![Mount for the reel board currently sitting under the big piece until have screws to mount it](image-15.png)
The above showcases the discussed piece that needs mounted

Next is what the wiring looked like before I started trying to clean it up
![Alt text](image-16.png)

Then what it looked like after:

![Alt text](image-17.png)

Then the pin that was causing us issue that we may have to babysit
![Alt text](image-18.png)

And finally the picture of the screen displaying the correct string once it got reseated correctly.

Overall, we still have a lot of code to do, but getting there on mechanical side. Mostly just need to get the coding done and all tied together, and get the box made for the secondary reel module, so we can at least show it in a demo state.

## Reel Box 11/17/23
So I went into the lab to check the new lid piece I made to properly account for the numpad, and was excited to see that it finally worked.

![Numpad fits](image-19.png)

I then worked to figure out where on the back of the main box with the machined parts we could mount that other base plate with standoffs I designed, and I came up with the following

![Alt text](image-20.png)

By putting it here, it is out of the way to be able to keep things clean, and then we can drill a 3/4 inch hole through the wood to run the cutter motor wire through, and then put the main control box and the reel box sitting on the workbench below it.
Also thought about whether we should do what the TA suggested to make it look nicer and pain the wood. Have not decided yet.

After that realization for the day, I was done for a bit, and came back to looking at it that evening.
Since we will not be able to build a second whole box with all the machined parts, so to proof of concept show that things work, we need a second box just to hold the PCB and attach the motors to. As a result, I started working on designing such a box. This was the result of said designs.

![OverallView](image-21.png)
![Visible side](image-22.png)

There is also a lid, but this way the information is on display to show that we can drive the other motors when requested, and make it clear which is which. This design was finished, but the endcap was designed seperately to make sure it would fit the motors, so it is being printed and tested before anything else. The overall design for this box takes about 10 hours to print. So once this is printed, the plan is to go in, make the hole, mount the one PCB, and test fit the endpiece with the motors.

## ReelBox continued 11/18/2023
I took the initial endcap piece in to confirm that the motors would fit.
I found they did, but that I may not be able to get all 4 screws in for the cutting motor. I determined that while an annoyance, that it should be fine. Given the following test:


![Motor testing on endcap](image-25.png)

Then I drilled a hole in the main wooden section and mounted the PCB stand t it. I ended up mounting the stand to one of the side posts as the screws I had with me were too long, and I realized it might make mounting simpler, resulting in the following:

![Hole drilled](image-24.png)
![Mounting](image-23.png)

Now that I determined the endcap should fit, need to print the rest of the box and the other details, and work on later assembly.

## Reelbox more 11/19/2023
Got the box printed, and assembled, need to take it in and test that everything fits

## Test fit Reelbox 11/22/2023
I took in the finished box to test it, and found that while my encap pieces fit the motors, the edge of the cutting motor hit the end of the box. As a result, I had to shift the design of the endcap moving the holes over further to the left so that the motors are shifted so the pieces will print. I reprinted the lid and I had brought the extra motors home, so I confirmed that it fit.

