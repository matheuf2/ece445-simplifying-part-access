[[_TOC_]]

# Aidan Worklog

## Design document preparation meeting - 9/27/23

Need to focus on R&V and adding details to the proposal doc. Working on block diagrams and initial schematics.

We are also settling on component choices for the design.

For the stepper, we are using a NEMA-17 0.9deg low-profile motor, which should still have enough torque for our
application. [link](https://www.amazon.com/STEPPERONLINE-Bipolar-Stepper-22-6oz-Extruder/dp/B00PNEQ79Q/ref=d_m_crc_dp_lf_d_t1_sccl_2_1/135-2508023-7486455?pd_rd_w=1GQuB&content-id=amzn1.sym.76a0b561-a7b4-41dc-9467-a85a2fa27c1c&pf_rd_p=76a0b561-a7b4-41dc-9467-a85a2fa27c1c&pf_rd_r=1Q8S4MT7DRXZQFTCK8YS&pd_rd_wg=etwmr&pd_rd_r=fe065b53-b1cc-445e-a6f6-736876ff494e&pd_rd_i=B00PNEQ79Q&psc=1)
This consumes 1A at 12V, which means that 3 of them would use 36W, which is higher than our initial 30W estimate.

We still need to decide on a motor to use for the cutter. This 12V motor from Amazon [link](https://www.amazon.com/Encoder-Gearmotor-130RPM-Arduino-Printers/dp/B07GNF3WTC/?th=1)
can be geared to very low speeds/high torques. Additionally, it only draws 600mA (according to the review), which adds 21W of additional peak power draw.
We could assume that the steppers are never moving at the same time as the motor, but the total of 47W is still very achievable.

Additionally, the logic supply will have a lot of components attached to it:
* ATMega328 = 6 mA x3 = 18 mA
* A4982 stepper driver = 8 mA x3 = 24 mA
* DRV8231 motor driver = 4 mA x3 = 12 mA
* 2x 7-segment LED displays (per reel module) = 5 mA x16 x3 = 240 mA
* ESP32 = 100 mA peak
* USB-PD controller = 3 mA
* Display = 37 mA
* Various other current sinks = 10 mA
* Total = 444 mA (1.47 W @ 3.3V)

## More design doc preparation - 9/28/23

The ATMega doesn't have a built-in encoder counter, so we would have to do it manually or switch to a different chip.
We should be able to do the counting in software if our loop is fast enough:

201 rpm * 21.3 gear ratio = 4280 rpm on the encoder shaft = 71.4 rev/s

Assuming 4 encoder counts per revolution, the main loop has to run at at least 285 times per second, which allows us somewhere between 10k and 40k instructions per loop cycle.
This means that we have plenty of processing power available to count the encoder in software.

## Reel module PCB design - 9/30/23

The reel module's ATMega processor has a *lot* of things attached to it:

### Power in/out

This is pretty simple; power goes directly from one module to the next one.
Not sure if a PTC fuse is needed.

### 12V sense input

This is a basic voltage divider to tell the processor whether the 12V rail is active, disconnected,
or having a moment. The processor can then disable functions to run the motors and tell the user or
main board that something is wrong.

It might be a good idea to have a zener diode to clamp this voltage at a maximum of 3.3V in case of
voltage surges, but this is a fairly minor issue.

### Reset button

This is an internal button to reset the MCU, with a pull-up resistor so that the pin doesn't float
and reset the processor all the time. This signal also needs to be routed to the ICSP header so
that the programmer can hold the chip in reset while programming it.

### Crystal oscillator

This provides a highly accurate clock signal to the processor so that it can keep the time intervals
for stepper commands and other time-sensitive actions within a tight tolerance.
The built-in RC oscillator has very high jitter, varies widely with temperature, and only runs at 8 MHz.

Since we are running the ATMega at 3.3V instead of 5V, we cannot clock it at the maximum 20MHz.

Between 2.7 and 4.5V, the maximum frequency increases linearly from 10 to 20MHz, so we can estimate
that at 3.3V,
```python
>>> m = (20 - 10) / (4.5 - 2.7)
>>> 10 + (m * (3.3 - 2.7))
13.333333333333332
```
Thus, we can safely run the processor at a maximum of 13MHz. We reduce this to 12MHz, which is an even
number and a fairly common crystal oscillator frequency.

From digikey we find a 12 MHz 18pF crystal. Using [Adafruit's handy reference](https://blog.adafruit.com/2012/01/24/choosing-the-right-crystal-and-caps-for-your-design/),
we can decide on a capacitor value using
```
C1 = C2 = 2*CL - 2*Cstray

- given CL = 18pF
- assume Cstray = 5pF

C1 = C2 = 36pF - 10pF = 26pF
- closest standard value is 27pF (can change by +/- a few pF if the lab doesn't have these)
- closest value in the ECE shop is 22pF (also close enough, Cstray = 7pF)
```

### ICSP port 

This allows the chip to be programmed over SPI. The pin-out is very specific, but KiCad provides a symbol
with the pins already named.

One thing to note is that the ATMega is typically used as a 5V device, so most ISP programmers apply
5 volts to the VCC pin. I'm adding a jumper to the ICSP Vcc so that 3.3V can be supplied externally
if the ISP programmer only has 5V output. (This is not idiot-proof, but if you're updating the firmware,
you should know what you're doing).

### Face buttons and LED display

The LED display is a bit of an interesting problem, mostly because I thought we did not have as many pins
as we actually do. We do need a shift register to send the digits to the display, but digit-select can
use two outputs instead of an inverter (and save on BOM cost).

The buttons can be connected directly from the ATMega pin to ground, since the chip has internal pull-up
resistors.

The shift register is a 74HC595 generic shift register, which has 3 inputs - DATA (the next bit), SHIFT
(clocks in data bits), and STORE (copies the shifted-in data onto the output pins). This allows us to
control all 8 LEDs (7 segments + 1 decimal point) with only 3 microcontroller pins. The digit-select pins
switch the digit that is being driven, which lets us use only one shift register for both digits at the
expense of the display running at a 50% duty cycle.

### Stepper/Motor drivers

These were fairly simple to implement although a lot of external passives were needed. We need high-power
(3/4 W or so) resistors to act as current-sense shunts for the motor/stepper drivers.

## BOM preparation - 10/3/23

Parts used (per board):
| Type |  Value       | Footprint         | Quantity | Digikey order (if not available from the shop) |
| ---: | -----------: | :---------------- | :------- | :--------------------------------------------- |
|    C |        100nF | 0805              | 7        |                                                |
|    C |         22pF | 0603              | 2        |                                                |
|    C |        220nF | 0805              | 2        | 1276-6478-1-ND                                 |
|    C |        100uF | Radial D10mm P5mm | 2        | (get from 2070 lab)                            |
|    J |    (various) | Molex KK-254 1x4  | 4        | WM4202-ND                                      |
|    J |    (various) | Molex KK-254 1x3  | 2        | WM4201-ND                                      |
|    J |  Motor\_Conn | JST-PH-K 1x6      | 1        | 455-1708-ND                                    |
|    J |    AVR-ISP-6 | Header 2x3 2.54mm | 1        |                                                |
|    Q | SSM6N7002KFU | SOT-363           | 1        | SSM6N7002KFULFCT-ND                            |
|    R |         5.1k | 0805              | 2        |                                                |
|    R |          33k | 0603              | 1        | CR0603-JW-333ELFCT-ND                          |
|    R |          12k | 0603              | 1        | RMCF0603FT12K0CT-ND                            |
|    R |          10k | 0805              | 5        |                                                |
|    R |         100R | 0603              | 2        | RMCF0603FT100RCT-ND                            |
|    R |   0.33R 3/4W | 2512              | 3        | PT.33UCT-ND                                    |
|   RN |      8x 120R | 1506              | 1        | Y1121CT-ND                                     |
|   RV |          10k | Bourns TC33X      | 1        | TC33X-1-103ECT-ND                              |
|   SW |   Pushbutton | 6mm Button THT    | 3        | (might be available in 2070 or ECE110/120 kit) |
|    U |ATmega328PB-A | TQFP-32           | 1        | (I will provide these chips)                   |
|    U |PI6ULS5V9517B | MSOP-8            | 1        | 31-PI6ULS5V9517BUEXCT-ND                       |
|    U |    LSHD-5503 | D1X8K             | 2        | 160-1576-5-ND                                  |
|    U |      74HC595 | TSSOP-16          | 1        | 1727-3068-1-ND                                 |
|    U |        A4982 | TSSOP-24          | 1        | (can harvest from lab e-waste)                 |
|    U |      DRV8231 | SOIC-8            | 1        | 296-DRV8231DDARCT-ND                           |
|    U |   LTH-301-05 | (Custom)          | 1        | 160-1935-ND                                    |
|    Y |   12MHz 18pF | HC49-SD           | 1        | 535-10218-1-ND                                 |

## Software - 11/7/23

After a lot of delays, soldering, exams, and preparation, we are finally at the point of software development.

There are a few things to calculate, of course, mostly related to timing calculations.

### Stepper step rate

The stepper motor rotates 0.9 degrees per full step. Thus, we need to quickly send step commands to rotate
the motor quickly.

* 1 s pulse = 0.9 deg/s = 0.15 rpm
* 100 ms    = 9 deg/s   = 1.5 rpm
* 10 ms     = 90 deg/s  = 15 rpm
* 1 ms      = 900 deg/s = 150 rpm

Since 1ms step rate is very reasonable procesing-wise, we can use a hardware timer+interrupt to control the stepper
with very tight timing (and resolution in increments of 8 CPU cycles). 

## Stepper testing - 11/12/23

The stepper motor works, let's figure out how fast it can go
| Time   | Rate (Hz) | Exp. RPM | Behavior
| ------ | --------- | -------- | --------
| 200 us | 5000      | 750      | Does not turn
| 500 us | 2000      | 300      | Sometimes turns but usually needs help
| 750 us | 1333      | 200      | Turns consistently

Thus, we can set 200 RPM as a safe limit for the stepper motor. Since the stepper loses torque at higher speeds,
we will run it at a lower rate (20-50 RPM) to minimize skipping.

I measured the timing of the stepper signal using an oscilloscope:

![Stepper signal waveform](stepper_waveform1.png)

This waveform confirms that the stepper rate is extremely consistent (since it uses a hardware timer)--this test
was run with a 1kHz step rate--and that the signal timing meets the stepper driver's timing requirements [1]:

![Stepper timing requirements](stepper_timing.png)

We can see from the lower zoomed-in waveform that the direction change (yellow trace) happens about 2
microseconds before the step pulse, and that the step pulse is about 2.5 microseconds wide. This is well beyond
the 1 us minimum step pulse width and 200 ns direction-input setup time.

(Note: the issue that created this waveform was that my code set the enable pin high when trying to enable the
stepper motor. Since the enable pin is active-low, this caused the stepper driver to do nothing when I expected
it to run.)

References:

[1] "A4982 Datasheet", Allegro Microsystems.
[https://www.allegromicro.com/~/media/Files/Datasheets/A4982-Datasheet.ashx](
https://www.allegromicro.com/~/media/Files/Datasheets/A4982-Datasheet.ashx) (accessed Sep 28, 2023).

## Comprehensive code changes - 11/30/23

Programming this machine has been a massive project, especially since I chose to write everything using
base-metal C code (no Ardunio library, just registers). Fortunately, there aren't very many I/O registers,
but this still required a lot of datasheet reading to make sure I was setting the bits correctly.

This log should document all of the program features I implemented for the reel module.

### Compiler toolchain and build system

To compile the reel module firmware, I made a Makefile to invoke `avr-gcc` for compiling and linking. I also added
some convenience targets: `make usage` runs `avr-objdump` to print the memory usage of the program, and
`make upload` runs `avrdude` to upload the firmware file to the microcontroller.

I also made a Python script to convert human-readable command line options into fuse bytes for the microcontroller.
These "fuse" bytes (they are reprogrammable) configure low-level parameters like clock input and whether certain
features are enabled on boot. This was fairly unnecessary (it was mostly a procrastination project), but it should
be extremely useful if I make any other AVR projects.

The fuse options used for this project are:

* `--bod-level=off` - the ATmega has 3 levels of brown-out detection (1.8 V, 2.7 V, and 4.3 V). Below these voltages,
the chip will hold itself in reset to avoid incorrect operation [2]. Since our project has a stable, regulated
3.3 volt supply (and not, say, a coin cell battery), I decided to disable the brown-out detection.

* `--clk-sel=cry-8m-slow` - The chip has a large number of options for clock inputs [2]. For this project, we choose
the fuse option for an 8MHz or higher crystal resonator, and the slowest start-up delay (to maximize reliability).

* `--div8-disable` - The chip divides the input clock signal by 8 by default [2]. I want it to run at the full
12 MHz though, so I disabled this divider.

### Pin definitions

Each I/O pin on the ATmega is controlled by the `PORT`, `PIN`, and `DDR` registers (our chip in particular has
4 sets of these--`PORTB`, `PORTC`, `PORTD`, and `PORTE`, etc. [2]) I made a `pins.h` file, which keeps track of
all of the pin assignments (which port and bit to use), as well as any other numbers (ADC index or interrupt index)
needed for other special-function pins.

I also defined some convenience functions to set and read pin values. 

### LED display driver

The reel board has a 2-digit 7-segment display to communicate debugging information. I made a simple API for this,
so that the program can either display a hexadecimal value (one byte) or directly set the segment values.
It also has an update function that pushes new segment data out to the shift register and then toggles the digit
selected by the digit-select outputs. If this function is called at a high enough rate, it will appear as if both
digits are illuminated at the same time. The main function calls this function using a hardware timer so that it
runs at a consistent 1 kHz.

### Hardware timers

I made the `timing.c` file to consolidate all of the timer initialization code. This software uses three of the
available hardware timers:

* `TIM1` is set up to trigger 1000 times per second and generate an interrupt each time. The counter maximum
  is calculated by the formula in the datasheet [2]:

  ![Timer 1 frequency](timer1_frequency.png)

  This can be rearranged into a formula for the value of OCR1A (the output-compare register):

  ```c
  const uint16_t period = (uint16_t)((F_CPU / (8 * freq)) - 1);
  ```
  (where `F_CPU` is the system clock frequency in Hz, and `freq` is the target of 1000 Hz). A 1/8 clock divider
  is used so that the target count value is less than the maximum of 65535.

* `TIM0` is set up as a PWM timer for the motor outputs. The OC0A and OC0B timer outputs are connected to the
  motor inputs, and the frequency is defined by the system clock divided by 256, or 46.875 kHz. This is because
  the PWM timer operates by counting from 0 to 255 and turning the output on until this counter exceeds the
  value stored in the output-compare register [2].

  ![PWM counting](pwm_counting.png)

  Since the same timer is used for both outputs, they run at the same frequency and phase, but the duty cycles
  can be independently controlled.

* `TIM3` is set up as an interrupt-triggering interval timer similar to `TIM1`, but there is a function to
  reconfigure the frequency on the fly. The rate-generation function is very similar to that of the `TIM1`
  timer, but it finds an optimal divider value that allows the maximum count value to fit inside the output
  compare register.

  This timer is used to send pulses to the stepper motor at a consistent but adjustable rate.

### Analog inputs

I made a very simple API to read from the analog input pins. This was intended to be used for the voltage-sense
input and the infrared sensor, but since the latter was scrapped, we only ended up using it for voltage sensing.

Although sampling the ADC only takes 250 microseconds [2], this is a fairly long time relative to other basic
functions (i.e., reading a digital pin only takes one instruction cycle, about 83 nanoseconds). As such, I noted that
this function should not be used inside an interrupt routine, since a "long"-running interrupt will hold up all of the
other interrupts in the system, especially since this project uses a *lot* of interrupts.

For voltage sensing, I calculated the expected input to the voltage divider when the pin read 3.3 V:

```c
// at 3.3V (reading = 1024), the divider input will be
// r = 12 / (12 + 33)
// 3.3 / r = 12.375 (volts)
```
I used this value as a conversion factor to map the 0-1024 range of the ADC to a more usable voltage measurement,
in millivolts for added precision (since this project only uses integer math).

### Motor Driver

The cutter motor is a simple brushed DC motor connected to a DRV8231 H-bridge motor driver. The driver takes in
two signals, `IN1` and `IN2`, and either drives the motor in a direction or stops it. As noted previously, I
am controlling these input signals using the `TIM0` timer in PWM mode, so that the speed can be adjusted by
changing the output duty cycle.

There is also a quadrature rotary encoder attached to the output shaft of the motor. This allows the position
(relative to its position when the system boots) and speed of the motor to be tracked continuously, which allows
for position control and stall detection in the final program. A simple state machine is attached to a pin-change
interrupt that checks if either of the encoder channels changed and updates the motor's position counter
accordingly.

### Stepper driver

The stepper motor is a small-form-factor NEMA-17 standard stepper controlled by an A4982 stepper motor driver.
Fortunately for us, the stepper driver handles all of the complexity of the control system (output sequencing and
microstepping) and exposes a very simple interface to the microcontroller, consisting of a STEP input and a DIR
input [3]. When a pulse is sent to the STEP input, the controller advances the stepper motor one step (or one
microstep, depending on the microstepping selection) either forward or reverse depending on the input to the DIR
pin.

I decided to put the stepping code into a hardware interrupt to ensure maximum consistency between steps. With this
design, the timer interrupt advances the stepper motor until a counter variable reaches its target. Thus, the
external interface controls the stepper with "set target", "advance target", and "check if target reached"
functions.

### I2C driver

The reel modules are set up as I2C slave devices. They receive commands from the control module and send status
information back when requested. Since the software has to respond immediately when the control module sends a
command, the I2C handling code runs inside the interrupt generated from the I2C controller hardware.

I2C is a fairly complex data protocol since it needs to transfer several different kinds of data over a single
data wire. Thus, the I2C hardware has many different states to handle reads, writes, acknowledges, and bus
errors. We can do some simplifying, though--if the software always acknowledges and returns data when requested,
we can ignore several states for when a not-acknowledge is sent.

Additionally, we need to keep track of the I2C address sent by the control module for daisy chaining purposes.
When the program starts, it sets its address to 0x7F and listens for a single-byte write command, which sets
its address. Then, once that I2C message has completed (to avoid confusing the next device on the daisy chain),
it enables the I2C passthrough chip to connect the next device to the bus.

Finally, the I2C interface uses a "register" system to allow the control module to specify what data is being
written. Writing one byte to the interface sets the register index (allowing for 256 registers), and subsequent
reads and writes access the byte stored in that register, if any.

The following shows the state changes and how the read and write transactions are handled.

![I2C state machine](i2c_flow.png)

Note: Atmel/Microchip calls the I2C interface "TWI" (Two-Wire Interface) since I2C used to be patented by
Philips [4]. Regardless, the TWI interface is identical to I2C, except for a few small differences that only affect
compatibility in extremely rare circumstances.

The capitalized names in the flowchart are either register names from the datasheet [2] or status values defined
in avr-libc [5].

### EEPROM control

For convenience, the reel module stores the reel component spacing value in non-volatile EEPROM memory so that
the user doesn't have to set this value every time the system is reset (they should only need to set it when
the reel is changed to a different component).

I added a couple of small functions to read and write bytes in EEPROM--these functions are practically identical to
the examples found in the ATmega328PB datasheet (page 36) [2].

Writing to EEPROM is a fairly complex operation internally, so it takes 3.4 milliseconds to write a single byte.
This may seem very fast, but this system already has an interrupt that runs every millisecond (and several others
in between that). Performing an EEPROM write inside an interrupt handler would hold up these interrupts and cause
very minor issues including lost time in the milliseconds counter and increased stepper jitter, so I added a note
to the header file to avoid using these functions inside interrupt handlers.

### Main program

Finally, after all of the driver code was completed, I created a state machine and some I2C register data 
to control the reel system.

There are two subsystems: the I2C register handler and the main state machine.

The I2C register handler keeps track of the following registers:

![I2C registers](i2c_reg_table.svg)

Note: the spacing value is a 16-bit integer that stores the distance to move to cut one component, in increments
of 1/100 mm.

When the control module updates various registers, the respective `UPDATE_` flag is set:

* `UPDATE_RUN` is set when a 1 is written to the Run bit of the status register. It will stop the running
  cut operation and reset the Run bit of the status register to 0.
* `UPDATE_CAL` is set when a 1 is written to the Cal bit of the status register. It will start a calibrate operation
  if nothing else is running.
* `UPDATE_CUT` is set when any value is written to the cut count register. It will start a cut operation if
  nothing else is running. If a calibration operation is in progress, the cut operation will be delayed until
  calibration is complete; if calibration has never been done, one will be started.
* `UPDATE_SPACING` is set when the high byte of the spacing register is written to. If nothing else is running,
  this new spacing value will be saved into EEPROM.
* `UPDATE_ERR` is set when a 1 is written to the Err bit of the status register. It will clear the error bit and
  return to a wait-for-command state.

The original control state machine (before any testing was performed) is as follows:

![Old state machine](ctrl_flow.png)

#### Main code testing

First, I ran the system in "test mode" without the main state machine and directly drove the motor with the buttons.
This allowed us to read the encoder value from the control module and then determine the distance that the motor
needed to move from its physical stop.

Next, I enabled the calibration function and made sure that it hit the end stop without jamming.

To test the main function, I modified the state transitions to perform a subset of the tasks.

* Cutter only: start cut -> `CTRL_DO_CUT` -> `CTRL_DO_CUT_REV`
  * This revealed that the motor movement direction was opposite the encoder counting direction (no
    surprise since I chose both fairly arbitrarily). After reversing the motor, this worked perfectly.
* Stepper only: start cut -> move stepper -> `CTRL_DO_STEP`
  * This had some difficulty with moving the motor without skipping steps (not due to the software but
    because of the torque required to move a full step). Increasing the current limit
    helped this a bit, but the theoretical precision of about 0.4 mm per step was very close to our
    requirement of 0.5 mm accuracy. Thus, we decided to enable 4x microstepping, which improved the
    accuracy significantly and vastly reduced the stepping noise.
* Stepper and cutter: start cut -> move stepper -> `CTRL_DO_STEP` -> `CTRL_DO_CUT` -> `CTRL_DO_CUT_REV`
  * This worked well, although we never calibrated the conversion factor from spacing units of 0.01 mm
    to stepper steps (which Matheu had calculated using the measured wheel radius), so it had a tendency
    to cut through the chips we were testing with (either cutting the legs off or cracking the plastic
    housing). This proved that our motor was more than powerful enough to cut through the tape, but it
    ended up dulling the knife to the point where it would start stalling on paper tape later on.
  * I observed that lining up the tape with the knife edge was very difficult while it was moved up,
    so I rearranged the state machine to move the knife down when not cutting.
  * I had also made all of the states that run the motor detect if it was stalled (if the position
    moved less than a few encoder ticks in a half a second, the code would jump to an error state
    and stop the motor).

While testing this mode, the motor started moving very short distances and then reporting a stall
condition.

* Checking the encoder test program, the encoder count was alternating between 0 and 1 when
  manually rotating it. This meant that one of the encoder channels was not reading properly, which
  could be an issue with the encoder itself (broken hall effect sensor), the cable (broken wire = floating input), 
  the connection on either end, or the microcontroller itself (the worst case scenario).
* To test the motor, we swapped it with the one on the secondary reel module. The problem persisted,
  and the original motor encoder worked perfectly when attached to the secondary reel controller.
* To test the cable, I checked continuity along every wire in the motor cable. Every wire had
  continuity, so this was not the issue.
* To test the connection, I only did a visual inspection, since actually testing the connection would
  require a lot of disassembly. Everything looked fine, so I moved on to the final conclusion--the chip
  had lost its input port functionality to electrostatic discharge.
* Fortunately, I had many spare ATmega328 chips on hand, so I went to the lab and replaced it. I also
  fixed the orientation of the stepper connector so that it was consistent with the other reel module
  board (we discovered that they were opposite each other when we swapped the reel boards and the
  stepper started feeding backwards with the same software).

The final state machine, after these changes and other bugfixes and quality-of-life updates, looks
like the following:

![Final control state machine](ctrl_flow_final.png)

Most notably, I added a `STALL_REC` state to automatically recover from a stalled cut by attempting to
reverse the cutter until it is fully open. Otherwise, if a cut operation stalls, the mechanism will jam,
which requires a lengthy process of manually backing the motor out and/or cutting or pulling the tape away.

The final program can report two errors to the control module. Error 1 is "low voltage", which is asserted if
the 12 V rail measures less than 6 volts during a run operation. This usually indicates that the power supply
connection is broken, or that it is not plugged in (i.e., if the system is being powered via a programming port).
Error 2 is "stall", and it is asserted if the cutter stalls. Due to issues with the cutter being dull/damaged,
the timeout for a stall condition has been extended to 10 seconds, but a one-second delay is probably sufficient.

### References

[2] "ATmega328PB", Microchip Technology. [https://ww1.microchip.com/downloads/en/DeviceDoc/40001906A.pdf](
https://ww1.microchip.com/downloads/en/DeviceDoc/40001906A.pdf) (accessed Dec 4, 2023). 

[3] "A4982 Datasheet", Allegro Microsystems.
[https://www.allegromicro.com/~/media/Files/Datasheets/A4982-Datasheet.ashx](
https://www.allegromicro.com/~/media/Files/Datasheets/A4982-Datasheet.ashx) (accessed Sep 28, 2023).

[4] "I²C", Wikipedia. [https://en.wikipedia.org/wiki/I²C](https://en.wikipedia.org/wiki/I%C2%B2C)
(accessed Dec 4, 2023).

[5] "avr-libc/include/util/twi.h at main", GitHub.
[https://github.com/avrdudes/avr-libc/blob/main/include/util/twi.h](
https://github.com/avrdudes/avr-libc/blob/main/include/util/twi.h) (accessed Dec 4, 2023).

## 12/4/23 - Quick code note

Previously, the error condition was impossible to clear due to an unknown bug. Today, I found the bug--turns out,
the error was cleared (the state machine returned to the `CTRL_WAIT_CMD` state), but it did not clear the error
bit in the status register. This caused the control module to still see the error code and continuously report
the error until the system was rebooted.

Sadly, it is well beyond the demo date now... fortunately, everything else functions properly.
