# Tejas Worklog

[[_TOC_]]

## Design Doc Prep - 9/27/2023

Working on adding R&V to the design doc, especially for the base module, and refining the reel module R&Vs with Aidan. Finalizing major parts to have a more concrete design doc.

### LCD display

Decided on using the Newhaven 128x32 monochrome and backlit [display](https://www.digikey.com/en/products/detail/newhaven-display-intl/NHD-C12832A1Z-NSW-BBW-3V3/2059235). Some of the motivations for this was it looks clean and has can display quite a few characters which will come handy when showing the reel name. It also can be interfaced easily using FSPI protocol on the ESP32.

### Rotary Encoder

For the encoder, going forward with what Aidan had recommended, the Bourns PEC16-4220F-S0024 [rotary encoder](https://www.digikey.com/en/products/detail/bourns-inc/PEC16-4220F-S0024/3534239) as it not only allows to use it as a rotary encoder but also has a switch in-built which should be useful for selecting a specific reel after enumerating through them.

### Keypad

Using a simple [keypad](https://www.digikey.com/en/products/detail/sparkfun-electronics/COM-14662/8702491) for this as currently the goal is to have numeric inputs. It might be useful for a larger scope project that has a more full-scale keyboard.

Used the datasheet to label rows and columns.

[Keypad_Pinout](https://cdn.sparkfun.com/assets/7/e/f/6/f/sparkfun_keypad.pdf?_gl=1*1m55xyu*_ga*MTQyNjQ5NDQ1LjE2OTM3MTcwMzI.*_ga_T369JS7J9N*MTY5NjIxODc4OS43LjEuMTY5NjIxOTE4Ny4yMi4wLjA.)

### Power supply

Worked on power calculations to make sure that enough power will be supplied by the current power supply of choice [link](https://www.digikey.com/en/products/detail/mean-well-usa-inc/RD-85A/7705988). It is an expensive part so we want to be sure that it will work for the project scope. There is a higher wattage supply but that will push the project cost even more which will put us quite a bit over the budget. A 5V and 12V output is quite desirable as the 12V is needed for the motors in the reel board and 5V can be easily stepped down. Finding a 3.3V and 12V combo is harder and not worth the extra design work. 5V will also probably be the input from the USB which will be used when programming so a 5V to 3.3V is needed anyways. This will also mean that all the power distribution has to be done through the base module and I have to keep the traces thick enough to allow all devices to draw the needed current.

## More Design Doc Prep - 9/28/2023

Worked further on the design doc, doing final revisions and fixing the schedule, dividing the work. Aidan has more experience with motors, so will take on the initial design of the reel module with me and Matheu helping with testing as needed. I will look into the cutting motor and work with the machine shop to ensure that we have a working plan on what blade will be used and how it will cut through.

Looked into IR sensors more and am concerned after the last TA meeting about the efficiency of [IR sensor](https://www.digikey.com/en/products/detail/sharp-socle-technology/GP1S093HCZ0F/720401) when it is a clear plastic. Maybe some other type of [sensor](https://www.digikey.com/en/products/detail/omron-electronics-inc-emc-div/EE-SX398/368614) could be used. For now and the design doc, we will let it remain as the IR sensor as that should cover a large range of reels. If after testing we have issues, this can improved on or written down as a possible contrainst in the current project scope.

Since I have previous experience with Arduino programming and IOT stuff, I will work on the UI for the control box.

## Datasheet review - 9/29/2023

Started reviewing the ESP32 manuals to understand pin assignments and how SPI and I2C devices need to be connected. Getting background information for the PCB design.

[ESP32-S3 WROOM datasheet](https://www.espressif.com/sites/default/files/documentation/esp32-s3-wroom-1_wroom-1u_datasheet_en.pdf)

## Base module PCB design - 9/30/23

### Power subsystem

Working first on the Power subsystem. Should be fairly straightforward with the power supply providing in 12V and 5V [datasheet](https://mm.digikey.com/Volume0/opasdata/d220001/medias/docus/2372/RD-85-SPEC-OLD_Ds.pdf). Bit concerned about the current limit of the 12V supply for the 3 steppers and 3 gear motors. Had a quick talk with Aidan who said based on his review of the datasheets for the motors, we should be below the limit.

Since the ESP32-S3 runs on 3.3V based on the datasheet, need to use a voltage regulator to go from 5V to 3.3V. Decided to use the one available from the ECE shop [link](https://www.digikey.com/en/products/detail/diodes-incorporated/AZ1117CD-3-3TRG1/4470979). This component also has enough current supply limit.

Ensured that the traces were 30 mils for the large power tracks with major load and 20 mils for the rest.

### ESP32-S3

After revisiting the main datasheet, imported a schematic and footprint for the ESP32-S3 WROOM module.

[SnapEDA](https://www.snapeda.com/parts/ESP32-S3-WROOM-1-N16R2/Espressif%20Systems/view-part/#)

Since the ESP32-S3 uses a GPIO matrix, it helps with the assignment of pins for the components. The keypad and encoder can take pins that are on the same side of the microcontroller and in a series physically. The LCD will be wired using the IO MUX table which tells us which pins are the IO MUX pins. I2C will be two pins assigned using the GPIO matrix.

Added an RC delay circuit to ensure power supply to the chip based on the datasheet schematics.

![RC_circuit](timing_circuit.png)

### I2C

Added a Molex connector for I2C out with SCL_OUT and SDA_OUT and GND.

### USB-C

Added a USB-C to the schematic as it will offer the advantage of better connection and connecting wire availability. To be honest, I just want to move away from the micro-USB connectors. Wired the CC1 and CC2 to 5.1k resistors and ground and two common signals from the two D- and two D+ signals. Not sure yet on what resistor needs to be attached to the shield.

![USB-C](usb_c.png)

### LCD

Attached the LCD directly on the PCB. Imported the schematic for it and the connection made using the datasheet. Decided to use all 1uF capacitors for the connection that a range provided for the compatible capacitors. This should help with ordering, soldering, and reducing complexity.

[LCD-datasheet](https://newhavendisplay.com/content/specs/NHD-C12832A1Z-NSW-BBW-3V3.pdf)

![LCD_Schematic](lcd_schematic.png)

## Further Base module PCB design - 10/1/23

### Micro-USB

Changed the USB-C to a micro-USB as it has less PCB design and soldering and we already have the part available from the ECE shop. Initially made the mistake of using the mini-USB schematic and footprint but fixed it as soon as I realized it. Connected the 5V output of the USB to the 5V input of the voltage regulator.

[SnapEDA](https://www.snapeda.com/parts/47346-0001/Molex/view-part/)
[Digikey](https://www.digikey.com/en/products/detail/amphenol-cs-fci/10118194-0001LF/2785389)

### LCD, keypad, rotary encoder

The keypad, display, and rotary encoder will be attached using Molex connectors. This should help later with where we place them on the control box. It should also reduce the size of the PCB itself and make it easier to solder. Also footprint of the encoder and keypad and not easily available.

The LCD has a pin pitch of 1.5mm while the Molex connector is of 2.54mm. Didn't see a 1.5mm connector at first look and is not a big issue if I have to solder wires directly on the ends.

Had a quick call with Matheu to ask if my connections for the LCD and ESP32-S3 were correct based on the tables for FSPI. He agreed with what I had. Also asked him to give an overview of the overall PCB layout.

![FSPI_table](fspi.png)

## BOM preparation - 10/3/23

Parts used:
| Type |  Value              | Footprint                 | Quantity | Digikey order (if not available from the shop) |
| ---: | ------------------: | :------------------------ | :------- | :--------------------------------------------- |
|    C |               0.1uF | 0805                      | 1        |                                                |
|    C |                 1uF | 0805                      | 9	    |                                                |
|    C |               10 uF | 0805                      | 1	    |                                                |
|    C |               22 uF | 0805                      | 2  	    |                                                |
|    J |   (Vin & Power_Out) | Molex KK-254 1x4 V        | 2 	    | WM4202-ND                                      |
|    J |            (KeyPad) | Header 1x7 2.54mm V       | 1        |                                     	     |
|    J |           (Encoder) | Molex KK-254 1x7 V        | 1 	    | WM4205-ND                            	     |
|    J |    	       (LCD) | Header 1x19 2.54mm V      | 1 	    |                                    	     |
|    J |           (I2C_Out) | Molex KK-254 1x3 V        | 1        | WM4201-ND                                      |
|    * |              Keypad | COM-14662                 | 1        | 1568-1856-ND                        	     |
|    * |      Rotary Encoder | PEC16-4220F-S0024         | 1        | PEC16-4220F-S0024-ND                           |
|    U |    	         LCD | NHD-C12832A1Z-NSW-BBW-3V3 | 1 	    | NHD-C12832A1Z-NSW-BBW-3V3-ND                   |
|    J |         Micro_USB_B | 0473460001     	         | 1        | WM17141CT-ND                                   |
|   VR |    AZ1117CD-3.3TRG1 | TO252-2-3.3V_DIO          | 1        | AZ1117CD-3.3TRG1DITR-ND                        |
|    R |                 10k | 0805                      | 1        | RMCF0805JG10K0TR-ND                            |
|    U |ESP32-S3-WROOM-1-N16 | 41-SMD Module             | 1        | 1965-ESP32-S3-WROOM-1-N16TR-ND                 |

## Psuedo coding - 10/14/23

Started on writing the psuedo code for the LCD, using the data sample initializing programs as my reference. Also found the Arduino library for the driver ST7565R which the LCD uses. Have backup code written using the library if my own code fails to run the LCD.

## Soldering - 10/23/23

Spent all of the evening soldering the parts we already have from the ECE shop. Left some parts to allow for other parts to come. Want to solder them first due to their placement. 

## Soldering continued - 10/30/23

Soldered on the rest of the parts. Only two capacitors are missing. Will wait a day or two and then will use two smaller capacitors to get what I need. Also got the micro-USB soldered on after a lot of issues. Got some solder inside it after melting from one end. Had to remelt it and the solder thankfully flowed out.

## Soldering finished - 11/3/23

Ended up just using two smaller capacitors to finish the soldering task. Falling somewhat schedule and really need to test the LCD.

## Software testing and more soldering - 11/4/23
Flashed the screen code but it seems to not be working, testing is constantly failing. 

Also quickly realized that I forgot to account for a reset button for the ESP32-S3. With help from Aidan, botched a push button on that pulls GPIO0 to ground(0) and GPIO46 is always 0. This puts the chip into Download Boot mode.

![Chip_Boot_Control](esp_reset.png)

## Keypad code - 11/5/23

Found a keypad library and leveraged it to write the code for the keypad. Will test it later. Realized that I have to use a polling method that waits for a valid character to be pressed when using the keyboard due to how the library is setup.

## LCD testing - 11/7/23

The LCD keeps failing even after trying to use the library code too after discarding the setup and comm_out and data_out code I wrote using the datasheet. Might switch to a 16x2 LCD display that Aidan has to get the project to a working state.

## Keypad testing - 11/8/23

Soldered the keypad on using a flexible wires and crimped them in female connectors. Tested the keypad code and it works. Tried using the method that doesn't wait for a valid character but it will make it integration later harder.

## LCD replaced - 11/9/23

Replaced the LCD and used the 16x2 LCD from Aidan. Took most of the evening to solder it, making sure to reuse as many of the data pins and having to run a wire from a new GPIO pin to an empty pad connected to the Molex connector as the new LCD needs more data pins. Had to also run direct wires from the 5V due to the new LCD needing it instead of 3.3V. Testing it was quite quick with the code being simple to write. The pins are somewhat loose and the solder work needs a rework.

[Pinout_for_new_LCD](https://www.circuitschools.com/interfacing-16x2-lcd-module-with-esp32-with-and-without-i2c/)

## Ordered new motors - 11/10/23

We are concerned that our current gear motor will not be able to cut through due to low torque. Spent time researching different motors available today and then I finally decided to order two new ones with 10RPM speed and 22.5kg.cm torque. These use the same encoder so Aidan shouldn't have to change his code.

[Amazon](https://www.amazon.com/dp/B08BL9D6BH?psc=1&ref=ppx_yo2ov_dt_b_product_details)

## I2C code - 11/11/23

Using the wire library created i2c write functions and setup function. Stores data about each reel in a struct array including index, two-byte spacing, and a flag if spacing is already stored. During setup, it writes to the default address over and over again as long as a device responds. After the first device, it writes an address of 0x01 to it and sets that as it's identified and now the default address is being used by the next reel module. It continues until no more modules are left and a NACK is received. Had to pull up the last reel module I2C so that it's not floating and pull the bus to GND causing timeout issues.

[I2C_referce_sheet_ESP32](https://espressif-docs.readthedocs-hosted.com/projects/arduino-esp32/en/latest/api/i2c.html)

## Overall integrated code - 11/12/23

Wrote most of the integrated code that uses all the components. Using polling for all tasks. The encoder is used to enumerate, then click to select reel, "#" is used for next and "*" for back.

## Rotary Encoder and I2C and overall function - 11/14/23

Researched a bit on how to interface with the encoder and found a library that makes it really easy to set up a range and iterate through the number range specified. Decided to use it over bare-bones code that only tells me the encoder value. Soldered on the encoder and tested the code. The encoder was initially not giving output so I did a waveform reading and it looks right.

![Encoder_Waveform](encoder_waveform.jpg)

Turns out there was just a pin assignment issue in my code. Fixed it and now it works!

Also tested the I2C code and it writes the address. Coordinated register command values with Aidan. Integrated with the main code. Tested it with reel modules daisy chained.

Tried to use the ESP exception decoder for debugging but was unsuccessful.[link](https://github.com/me-no-dev/EspExceptionDecoder)

## Final testing for Mock demo - 11/15/23

Got the pieces from the machine shop and tested the complete code on them. The code works as intended! 

## Addition i2c functions and error checking- 11/25/23

Added a read function. It works similarly to the write functions by first writing the register address and then instead of writing to the register, it sends a readFrom command. Use it to do error polling, and check run availability.

On startup, the ESP32 also checks if the spacing is stored on the reel board and reads it in. It also displays it under the reel name.

## Added Repeat and motor refinement - 11/26/23

Now have added repeat functionality which allows multiple cuts of a specified quantity. It is implemented on the base module level and it checks the run availability of the reel module before sending the next cut command. Also now shows what error has occurred based on the error cut in the 4 most significant bits of the status register with the option to send the error cleared command to the reel module using "#".

Helped Aidan refine his motor values for the cutter motor using a test program to read its values through I2C.

Based on the physical limitations of space in the cutting area, the maximum quantity that can be dispensed for that reel is displayed next to the quantity menu. 

## Added motor calibration command - 11/27/23

Now during setup, the base module writes to the calibrate bit in the status register to put the motor at the right place and give it spacial reference. Spent most of the day testing out the overall code.

Added kill switch functionality on a software level which the user can cancel the current operation by pressing the "*" key. 

Also, spacing can now be edited by pressing "0" as the first character on the repeat or quantity menu.

## Demo - 11/28/23
All the functionality was working with the motor stalling a few times. Also realized the code to display what error had occurred was not working correctly. Fixed it after the demo and now it shows a "Stl" or "Vlt" error. Although this didn't have any effect on functionality. Error clearing was also flawed on both the base and reel modules. Fixed it on my end. The final flow of the program is as follows:

![Base_Module_Overall_Functionality](base_module.jpg)

## References
[1](https://www.espressif.com/sites/default/files/documentation/esp32-s3_technical_reference_manual_en.pdf) - https://www.espressif.com/sites/default/files/documentation/esp32-s3_technical_reference_manual_en.pdf

[2](https://espressif-docs.readthedocs-hosted.com/projects/arduino-esp32/en/latest/api/i2c.html) - https://espressif-docs.readthedocs-hosted.com/projects/arduino-esp32/en/latest/api/i2c.html

[3](https://www.espressif.com/sites/default/files/documentation/esp32-s3_datasheet_en.pdf) - https://www.espressif.com/sites/default/files/documentation/esp32-s3_datasheet_en.pdf

[4](https://www.bourns.com/docs/Product-Datasheets/pec16.pdf) - https://www.bourns.com/docs/Product-Datasheets/pec16.pdf

[5](https://www.espressif.com/sites/default/files/documentation/esp32-s3-wroom-1_wroom-1u_datasheet_en.pdf) - https://www.espressif.com/sites/default/files/documentation/esp32-s3-wroom-1_wroom-1u_datasheet_en.pdf

## Final Presentation prep - 12/4/23

Practiced the presentation with the group.