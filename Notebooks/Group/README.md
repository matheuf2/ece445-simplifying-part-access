# Group Worklog

[[_TOC_]]

# 2023-09-12 - Discussion with Gregory Jun

Met with our TA Gregory Jun for initial discussion around the project. The key insides he gave us were to:
* Focus on reel module
* Start with potentiometer for reel module to determine output
* Focus on accurately controlling stepper motor
* Have someone focus on cutter
* Lean towards stepper refined rather than IR sensor
* Then start thinking about UI
* Within next week, deciede on motor and cutting mechanism

# 2023-09-15 Meeting with Machine Shop Supervisor Gregg Bennet

A couple of us met with Gregg to start initial conversation with machine shop in case anything would be needed from them. Our primary concern had been the cutter.
He gave us some extra insight about how to approach the rest of the project.
Primarily, he thought we were in a pretty good place over all, and to decide on details and meet with him often to refine things.
Namely when asked about the cutter. He thought of it like a paper guillotine using standard razer blades like one of us had considered would work, with a gear motor behind it, where it feeds through and drives with  a hingle. Particularly, to choose such a motor with precision control and an ecoder is a must. Additionally, in agreement of having a kill switch somewhere since that is the one danger point of the project.

His primary concern was that if we do it with rollers to pull the tape, that the components tend to stick up from the tape, so having the rollers right to not damage them, make enough contact as needed, and getting level so it does not bunch up to one side or the other. But he said that is something he could try to help us brainstorm solutions for.

The goal he set for us was to reach out to Jason and actually get the spools they are using from him, such that we can figure out the details of the box and everything we need around that.
He also said the ECE shop would love something like that he knows.

# 2023-09-20 Meeting with Gregory Jun
We met again with our TA Gregory.
The main points he made was checking in that we get
* motor microstepping figured out
* Start working towards prototype for refinement of precision

He suggested that for the motor system, Do it more like a printer , and have a motor at the bottom for the first motor and roller, and use a spring loaded roller on top to help feed it on the top, and user other spring loaded stuff on the side to stabilize it.

Also the ideas of little trays in front of the modules to dispense it further.
Similarly, if we can not fully finish, another team can continue from our work.

Similarly, he is thinking maybe work with machine shop for gearing, rather than relying on microstepping for increased precision.

(Clean these notes up some more later)

# 2023-09-25 Meeting with Gregory Jun
Met with TA, discussed when scheduling design review. that will be 2 TA's and a professor.

During conversation we continued to discuss whether or not to use gears or microstep the motor. Still looking towards normal nema17 motor like other examples we have seen.
One issue we realized is with the IR sensor, that it would not work very well for clear tape as it would constantly be open. Need to check how IR behaves with clear plastics.
Additionally, if utilizing an analog sensor, could get more refinement, could try to detect the dips, but is more prone.
Could try something with laser to handle but could be tricky.
Another possible approach would be using gears that the sprocket slightly catches, and detect rotation to determine. But that could be tricky if it catches.
Maybe using the encoder or something out of a mouse scroll wheel.

He said he would get the reel to us by wednesday

He was hoping by next week get the order in for the stepper motor, put order in for the components, and go with the IR sensor for now, and we can see if works or now. Get microcontrollers and everything by next wednesday.

Hoping by November can start working more on nice to haves.

# 2023-10-02 Meeting with Gregory Jun
Big points of conversation in this meeting was mostly about us actually getting the orders in for the parts so we can really start prototyping.
He is not too worried about us given that our design does not have a lot of unknowns, mostly a lot of just prototyping and refining.

We can also resubmit the proposal by Monday to refine it some more.
He took the points off for tolerance analysis. Basically that the value itself was too wide, and to tighten that, and discuss beyondjust microstepping, like add information about the feedback mechanism (the IR sensor). Basically add that, and add some more to the tests for the individual systems (as we later do in the design doc), and should be able to get the 3 points back.

He was also thinking like once we set up control for it, so we can start testing the IC2 quickly before we have stuff fully implemented.

Also big comment of being just about getting the parts actually ordered so we can truly start prototyping motors and everything!

Also about making sure handle the modularness to be able to handle the various widths.


Another idea we just thought of for handling the modularity, is to make the bottom roller that is motorized much wider than needed, and then simply make the top springloaded part interchangable with the top roller and the side guides such that it should stay consistently.

Maybe for the connects maybe try to use the connectors from a power supply.

Also the point that Gregory pointed out was about us adding feedback for the cutting motor so we can tell if it's facing extra resistance to tell if it is trying to cut through a component.

Also suggestion of using micro-usb rather than usb-c for implementing the programming of the microprocessors.

Still big take away is lets actually place the parts ordered!

# 2023-10-09 Meeting with Gregory Jun
Met with the TA, since it had been a busy week, we did not really have much to add, as we had been working on the PCB.
He mostly told us to get that wrapped up, and to make sure we have parts ordered.
Additionally, work had been done to revise the proposal, so we told him about that portion, so he could review it.
Other than that, was not much to add.

# 2023-10-16 Meeting with Gregory Jun
Full group met with the TA.
Normal conversation of making sure we have the parts ordered.
Conversation about making sure we had the PCB ordered and that we know how to solder it.

We need to figure out in advance how to start programming and designing it.
Since difference between ESP32 and arduino is mostly pinouts, like figuring out how to program display and that.

Additionally, I mentioned the conversation with machine shop about not 100% being sure about whether we will have 2 parts.

As for points we lost on the design document.
Two of the 4 points lost was mostly regarding tolerance analysis, not being particularly sure that it will be tight enough. He was like adding experiment results would  be main way.

What he remembered was lacking just say put in whole reel and want to cut in intervals of 2, there's gonna be some issues, so have like 90% or 95% yield stuff to help better explain our tolerances. So putting something like that in our high level requirements.

Hoping that by mock to at least have detail of the code and the feed part working for the mock demo at least.

Additionally, he asked to make sure for like the stepper drivers and that, that we have enough heat dissipation for it.
Similarly, just that we've got stuff designed for possible OCP or reverse polarity.

Talked more about maybe for next week start working on try to get keypad and rotary encoder working to modify screen as a baseline a bit.

Also figuring out how exactly to design box and details. Maybe it doesn't need to actually have to be fully enclosed, so it can print faster, and simpler.
He was also pushing that we actually have two boxes rather than One box and one kinda in pieces working to show it works, as it looks better for final project.

Overall, mostly just trying to figure out how we can get stuff shown to actually get it to work for Mock demo and actual demo.

# 2023-11-01 Meeting with Gregory Jun
Group had arranged to move our normal weekly meeting with the TA to Wednesday. In discussion, we talked about where we were, having finished soldering the boards. The main thing is us missing the two microfarad capacitors we still need for the main control board.
At this point the heavy focus is on software integration, and assembly, and assuming everything works as expected.
Also prepping for the mock demo.

# 2023-11-08 Meeting with Gregory Jun
Discussing trying to find for the demo. Our general idea is just trying to get the code integration between the main and control board, just so we can showcase that we are able on the control board to request a certain amount of cut, and show if the stepper motor feeds and the other motor spins to cut it, even if we don't actually have the full setup done.

Additionally, just us trying to figure out how to handle assembly in preparation for final demo too.
So hopefully if the machine shop at least if one is fully done, we can show the dummy module and the actual one working, and swap them out.

Also discussing as a whole, we found out the machine shop had started more work on it, so hopefully we should have the parts by then.

# 2023-11-15 Group Work and Mock Demo
Matheu and Tejas met to check in with machine shop after lunch. Skee had it mostly done and needed roughly another hour, so we went upstairs to the lab, test fit the screen and pieces. found one of the knobs worked, and that the screen fit on the new piece, but in making the taller version of the box, an issue had occured with the sizing of the keypad, which resulted in it not fitting. That simply needs a cutaway that needsedited on the model and printed

Then Tejas and Aidan worked for the afternoon setting stuff up, and got the pieces from the machine shop, and working on I2C and cables. Matheu came back around 3 and spent the next couple hours helping clean things, up, crimping and creating cables while Aidan worked on power terminals, and Tejas coded.

# 2023-11-26 Group Assembly and coding
The three of us met to try to finish up assembly and try to get code in a state where we can finally test it before demo on Tuesday (the 28th). Initially Matheu found and got concerned that he made the reel module box too short on the top and as such we would not be able to fit the molex connectors. Then Aidan and matheu determiend if we flip the board and make that the second module, then we would not need the molex connectors there, as we simply have a pullup resistor attached on that board to close the I2C connection. Additionally Matheu realized when he made the cutout in the lidfor the LED's to be visible that they were aligned wrong because when he pulled the dimensions from Kicad, he forgot the board would be mirrored since it was on the back half of the board.
As a result, he discussed whether or not to reprint it, but after discussion Aidan and Matheu were leaning against it due to the fact the LED's were mostly for debugging anyway. Matheu worked on contiung assembly, and Aidan and Tejas discussed the code, so we can try to get in a testable state.

During assembly, we also realized that the smaller reels don't exactly work as the paper tapes are slightly thicker and don't fit within the mechanical parts we have straight enough (due to extra thickness and being slightly narrower). As a result, we are worried about the testability of that part of our higher level requirements.

## 2023-11-26 Group work continued.
We got the code working to the point that we could do basically testing of the cutter motorpowering the reel module directly, and powering the cutter motor through the power supply (for the 12V). After proving that we were able to see the value of the encoder on the cutting motor, and fine tuned the speed of the motor, we connected it to the actual mechanical module and were able to cut through the tape when we manually fed it to the right location. In testing, we wanted to confirm it would work with the reel with tighter spacings. In doing so, we ended up off with our manual feed and managed to cut directly through the IC, which actually made us happy as it means that we have more than enough power in the cutting motor (as that has been one of our main concerns).

We did run into some other issues, like the smaller tapes also like to try to bent up at the end because of how they fit through, and additionally, we need to figure out how to adjust the roller so it's able to mesh properly with the tape.

It is still necessary to test the individual sections of the code and test that the I2C works, that the stepper driving still works, and that things are generally behaving.

We decided to quickly do a rough test just using a button to test the stepper motor alongside the feeding, and that most of the other work will be continued later. We confirmed it worked correctly with the bigger reel.

## 2023-11-27 More code work.
We continued wok testing, we were able to make requests from the control box to the reel module with an older version of our code, but the stepper was feeding an incorrect amount. We found one bug which was a lack of parentheses before a bit shift caused the specified spacing to be wrong when sending it from the Base module to the Reel module, and upon changing it, the numbers made a lot more sense, but still needed some modification. Additionally, we found it was not consistently feeding multiple chips but would feed single ones at a time well enough. We realized that due to speed, it is because of having the current limiter for the motor set too low. We attempted to slightly increase it, and suddenly it was behaving perfectly fine.

The next biggest issue was tying the calibration of the cutter motor, then the feed and cut operations well enough. We ended up cutting through another chip (this time through the legs). We know its due to the precise spacing of the chips on the Blue reel, and that how tight it is makes it hard for our thing. As a result, a discussion occurred about whether or not to solder the jumper and do microstepping (as it would help), but we had some degree of uncertainty, as based on our calculation, for a spacing of 7.84mm between chips, and a feed of .43mm per step (without microstepping), it takes 18.2 steps to feed. If we were to microstep it to half that.
Suddenly in the process of testing and deciding this, the cutter motor stopped behaving as expected. We quickly realized this was due to the  rotary encoder misbehaving. In attempting to further diagnose, we dismantled the cutting mechanism, swapping the motor with the motor from the other module, and found we still had the same problem. Next we continuity tested the wire to the motor, and had the continuity for all pins, so it was not the wire. Unfortunately, we realized that this meant it had to be the PCB / the atmega chip itself. We then swapped the board with the other reel module board, and the rotary encoder was back to behaving.
Given this, Aidan decidedhe would work on trying to desolder the current atmega, and solder on a newone in the hopes that it will work. Additionally, we realized the header for one of the motors was also backwards, so he was also planning to desolder and resolder that header back on too.

We continued trying to test, and eventually came to a number of problems.
Namely the screen stopped displaying things (either the data pin came lose, or something else is misbehaving). As a result, nothing is behaving. We believe that the second pin it seems that the solder broke and so it's got a lose connection, which is causing the problem... plus when it comes back, a datapin is having problems resulting in gibberish.

On a different note, we found we also don't have the correct bit with us to try to adjust the height of the roller, which in turn means that cannot currently feed the other tapes. We will have to bring an allen key set to try fix that in the morning.

Aidan got back with the chip and it powers on. Moving back to the lab to test more.

## 2023-11-27 into 2023-11-28 Back in the lab.
We moved back to the lab, as we began having issues with the screen not displaying anything correctly due to a wire coming lose / shorting with another wire (both of which occured). Aidan decided to entirely resolder the screen connection with a ribbon cable. While he did that, Tejas was continuing to work on code, Matheu soldered a microstepping jumper and worked on assembly. We were finally able to adjust the height of the roller for the stepper motor so it would mesh with the thinner tape.
We got the screen back, confirmed it was working, and then in trying to assemble it, we found the entire top row of the numpad suddenly stopped working, due to a couple wires that ripped out. As such, after a couple more rounds of fixing that, we were finally able to have the whole box assembled. On first assembly, the screen showed nothing and we freaked out, but we quickly realized the reel modules weren't trying to do anything, which meant we simply had not properly flashed the control board. Undoing the main box to reflash it, we confirmed that was the problem, reflashing and reassembling it. Next, we tried cutting off parts, confirming we could set and change the spacing and everything. It worked great, with the exception that we still had some slight precision issues. At this point though, it was 5am and we called it for demo later in the day. 

Demo then mostly went okay, with some errors, and realizing we couldn't clear the errors due to forgetting to flash the new code with a single line changed. Overall we still felt like we were mostly able to show the progress of the project though.

